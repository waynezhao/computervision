// ----------------------------------------------------------------------------
// Class for representing MRF network for segmentation problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sat,Apr 19th 2014 09:54:05 PM EDT
// Last Modified: Sun,Apr 20th 2014 02:12:19 PM EDT
// ----------------------------------------------------------------------------
#ifndef _SEG_MRF__H__
#define _SEG_MRF__H__ 

#include"segMRFNode.h"
#include"SImage.h"

class segMRF{
    public:
        segMRFNode **allNodes;      // pointer to all MRF nodes
        SDoublePlane *img;          // pointer to original image img[0],img[1],img[2]
        int maxIter;                // maximum number of iteration of LoopyBP
        int numStates;              // number of possible states for hidden node

        //----------
        // methods
        //----------
        // constructor
        segMRF(SDoublePlane *image, int num, int maxIterations = 100);
        // deconstructor
        ~segMRF();

        // loopy belief propagation
        SDoublePlane loopyBP( const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar );
        // modified loopy belief propagation
        SDoublePlane loopyBP( const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar, const vecDoubles &bgMean, const vecDoubles &bgVar);

};
#endif
