#include"SImage.h"
#include"SImageIO.h"
#include<vector>
#include<iostream>
#include<fstream>
#include<map>
#include<math.h>
#include<cstdlib>
#include<string>
#include<sstream>
#include"dispMRF.h"
// for creating dir
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>


int main(int argc, char *argv[]) {
    if(argc != 4 && argc != 3) {
        std::cerr << "usage: " << argv[0] << " <image_file1> <image_file2> [gt_file]" << std::endl;
        return 1;
    }

    string input_filename1 = argv[1], input_filename2 = argv[2];
    string gt_filename;
    if(argc == 4)
        gt_filename = argv[3];
    //-------------------------
    // extract name of figure
    //-------------------------
    unsigned pos = input_filename1.find_last_of("/");
    std::string figName = input_filename1.substr(0,pos);
    std::string fileName = input_filename1.substr(pos+1);
    pos = figName.find_last_of("/");
    figName = figName.substr(pos+1);
    pos = fileName.find_last_of(".");
    fileName = fileName.substr(0,pos);

    //--------------------------------------
    // create result dir and sublevel dirs
    //--------------------------------------
    std::string resDir="results";
    std::string steDir=resDir+"/stereo";
    std::string imgDir=steDir+"/"+figName;

    //-------------------
    // check if dir exist
    //-------------------
    struct stat st;
    if (stat(resDir.c_str(), &st) == -1) {
        cout<<"creating dir: --> "<<resDir<<endl;
        mkdir(resDir.c_str(), 0755);
    }
    if (stat(steDir.c_str(), &st) == -1) {
        cout<<"creating subdir: --> "<<steDir<<endl;
        mkdir(steDir.c_str(), 0755);
    }
    if (stat(imgDir.c_str(), &st) == -1) {
        cout<<"creating subdir: --> "<<imgDir<<endl;
        mkdir(imgDir.c_str(), 0755);
    }

    //------------------------
    // read in images and gt
    //------------------------
    SDoublePlane image1 = SImageIO::read_png_file(input_filename1.c_str());
    SDoublePlane image2 = SImageIO::read_png_file(input_filename2.c_str());
    SDoublePlane gt;
    if(gt_filename != "") {
        gt = SImageIO::read_png_file(gt_filename.c_str());
        // gt maps are scaled by a factor of 3, undo this...
        for(int i=0; i<gt.rows(); i++) {
            for(int j=0; j<gt.cols(); j++) {
                gt[i][j] = gt[i][j] / 3.0;
            }
        }
    }

    //----------------------
    // do stereo using mrf
    //----------------------
    int maxIterations= 50;
    int windowSize= 5;
    int scale = 3;
    int dispLevel = 256/scale;
    dispMRF mrf(&image1,dispLevel, maxIterations);
    std::cout << "Creating MRF with max iterations: "<<maxIterations<<" and window size: "<<windowSize<< std::endl;
    SDoublePlane disp3 = mrf.loopyBP(image2, windowSize);
    for(int i=0;i<disp3.rows();i++) {
        for(int j=0;j<disp3.cols();j++) {
            disp3[i][j]*=scale;
        }
    }
    // result file name
    std::stringstream ss;
    ss<<windowSize;
    std::string rstName = imgDir+"/"+fileName+"MRFDispWnd"+ss.str()+".png";
    SImageIO::write_png_file(rstName.c_str(), disp3, disp3, disp3);

    //-------------------------------------------------------------
    // Measure error with respect to ground truth, if we have it...
    //-------------------------------------------------------------
    if(gt_filename != "") {
        double err=0;
        for(int i=0; i<gt.rows(); i++) {
            for(int j=0; j<gt.cols(); j++) {
                err += sqrt((disp3[i][j] - gt[i][j])*(disp3[i][j] - gt[i][j]));
            }
        }
        std::cout << "MRF stereo technique mean error = " << err/gt.rows()/gt.cols() << std::endl;

    }

    return 0;
}
