// ----------------------------------------------------------------------------
// Class for reprensenting a MRF node for segmentation problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sat,Apr 19th 2014 09:47:37 PM EDT
// Last Modified: Sun,Apr 20th 2014 03:44:20 PM EDT
// ----------------------------------------------------------------------------
#ifndef _SEG_MRF_NODE__H__
#define _SEG_MRF_NODE__H__ 
#include"split.h"

// define direction macro
enum DIRECTION {UP, RIGHT, DOWN, LEFT };
// Parameter ALPHA for energy function
#define ALPHA 3

class segMRFNode {
    public:
        int numStates;              // number of states for hidden node
        double* evidenceMsg;        // evidence message
        double** oldPairwiseMsg;    // old pairwise message of 4 directions from iteration t-1
        double** newPairwiseMsg;    // new pairwise message of 4 directions from iteration t
        int finalState;             // final state for MAP query
        Point myPos;                // position for this node
        segMRFNode **allNodes;      // all other nodes belong to this MRF
        SDoublePlane *img;          // pointer to original image img[0],img[1],img[2]

        //----------
        // methods
        //----------
        // constructor
        segMRFNode(Point pos, segMRFNode **allNodes, int numStates, SDoublePlane *img);
        // copy constructor
        segMRFNode(const segMRFNode &nd);
        // destrctor
        ~segMRFNode();
        // assignment operator overload
        segMRFNode& operator= (const segMRFNode & nd);

        // fill in evidenceMsg
        void getEvidenceMsg(const ptSet& fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar);
        void getEvidenceMsg(const ptSet& fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar, const vecDoubles &bgMean, const vecDoubles &bgVar);
        // get message from neighbor nodes. dir can be UP, RIGHT, DOWN, LEFT
        void getMsgFrom(int dir);
        // send message to neighbor nodes. dir can be UP, RIGHT, DOWN, LEFT
        void sendMsgTo(int dir, double* msgBox);
        // decide MAP state
        void decideMAPState();
        // a wrapper for calling getMsgFrom 4 times
        void getAllMsg();
        // update old message box with new message box
        void updateMsgBox();
    private:
        // zero out message (set particular message to zeros)
        void zeroMsg(double* msgBox);
        // check if a point is out side of image range
        bool outOfRange(Point pt);
        // pairwise cost function
        double pairCost(int i, int j);
};
#endif
