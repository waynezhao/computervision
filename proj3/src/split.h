//----------------------------------------------------------------------------
// This file contains function and datastructure used for image segmentation
// (step 2, 3)
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sat,Apr 19th 2014 02:00:07 AM EDT
// Last Modified: Sun,Apr 20th 2014 04:58:18 PM EDT
// ----------------------------------------------------------------------------
#ifndef _SPLIT__H__
#define _SPLIT__H__
#include<boost/unordered_set.hpp>
#include"SImageIO.h"
#include"SImage.h"
#include<string>

// constants for fore, and back ground
#define FORE_GROUND 1
#define BACK_GROUND 0
#define PI 3.1415926535897932

// beta in unary cost function
#define BETA 16.5 

// representing a 2D point
class Point {
    public:
        int row, col;
        // cosntructors
        Point() {} 
        Point(int _r, int _c);
        // copy constructor
        Point(const Point &pt);
        // assignment overload
        Point & operator= (const Point &pt);
        // this function is needed for hash_combine()
        bool operator==(const Point &p) const; 
        // if a point is in a set 
        bool ptInSet(const boost::unordered_set<Point> & s);
        // print
        void print();
};

// need to define a hash_function for class point to use unordered_set
std::size_t hash_value(Point const &p);

// save some typing to define 'ptSet'
typedef boost::unordered_set<Point> ptSet;
typedef std::vector<int> vecInts;
typedef std::vector<double> vecDoubles;

//---------------------------------------------------------------------------
// Take in an input image and a binary segmentation map. Use the segmentation
// map to split the input image into foreground and background portions, and
// then save each one as a separate image.
//---------------------------------------------------------------------------
void output_segmentation(const SDoublePlane *img, const SDoublePlane &labels, const std::string &fname);

//--------------------------------------------------------------------
// Given a set of pixel position (foreground or background)
// Return the mean (meanSet) and variance (varSet) for (R G B value)
//--------------------------------------------------------------------
void getMeanAndVariance(const SDoublePlane *img, const ptSet &pos, vecDoubles &meanSet, vecDoubles &varSet);

//---------------------------------------------------------
// Given mean and variance, and target
// Return the probability of target P(target | mean, var)
//---------------------------------------------------------
double gaussian(double target, double mean, double var);

//---------------------------------------------------------------------------
//-------
// Given:
//-------
//      1) input image 
//      2) position of manually-labeled foreground and background pixel
//--------
// return:
//--------
//      1) "label matrix": the same dimension as input image. Each element of
//      this matrix represent if the corresponding pixel is foreground or
//      background
//---------------------------------------------------------------------------
SDoublePlane naive_segment(const SDoublePlane *img, const ptSet &fg, const ptSet &bg);

//----------------------------------------------------------------------------
// Modified version of naive_segment, assume background pixels also follow a
// gaussian distribution
//----------------------------------------------------------------------------
SDoublePlane naive_segment_mod(const SDoublePlane *img, const ptSet &fg, const ptSet &bg);

// Unary cost function, this is the D() function in step 2
double unaryCost(int label, const SDoublePlane *img, Point pt, const ptSet &fg, const ptSet &bg, const vecDoubles &mean, const vecDoubles &var);

// modified unary cost function, assume not only foreground but also background
// follows gaussian distribution
double unaryCostMod(int label, const SDoublePlane *img, Point pt, const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar, const vecDoubles &bgMean, const vecDoubles &bgVar);

// print a double vector of length len
void printMsg(double* msg, int len);
#endif
