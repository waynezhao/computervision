// ----------------------------------------------------------------------------
// Class for reprensenting a MRF node for stereo problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 20th 2014 05:10:54 PM EDT
//          Copy from dispMRFNode.h/cpp
// Last Modified: Sun,Apr 20th 2014 10:53:34 PM EDT
// ----------------------------------------------------------------------------
#ifndef _DISP_MRF_NODE__H__
#define _DISP_MRF_NODE__H__
#include"split.h"

// define direction macro
enum DIRECTION {UP, RIGHT, DOWN, LEFT };
// Parameter ALPHA for energy function
#define ALPHA 5

class dispMRFNode {
    public:
        int numStates;              // number of states for hidden node
        double* evidenceMsg;        // evidence message
        double** oldPairwiseMsg;    // old pairwise message of 4 directions from iteration t-1
        double** newPairwiseMsg;    // new pairwise message of 4 directions from iteration t
        int finalState;             // final state for MAP query
        Point myPos;                // position for this node
        dispMRFNode **allNodes;      // all other nodes belong to this MRF
        SDoublePlane *img;          // pointer to original image img[0],img[1],img[2]

        //----------
        // methods
        //----------
        // constructor
        dispMRFNode(Point pos, dispMRFNode **allNodes, int numStates, SDoublePlane *img);
        // copy constructor
        dispMRFNode(const dispMRFNode &nd);
        // destrctor
        ~dispMRFNode();
        // assignment operator overload
        dispMRFNode& operator= (const dispMRFNode & nd);

        // fill in evidenceMsg
        void getEvidenceMsg(const SDoublePlane &targetImg, int wndSz);
        // get message from neighbor nodes. dir can be UP, RIGHT, DOWN, LEFT
        void getMsgFrom(int dir);
        // send message to neighbor nodes. dir can be UP, RIGHT, DOWN, LEFT
        void sendMsgTo(int dir, double* msgBox);
        // decide MAP state
        void decideMAPState();
        // a wrapper for calling getMsgFrom 4 times
        void getAllMsg();
        // update old message box with new message box
        void updateMsgBox();
    private:
        // zero out message (set particular message to zeros)
        void zeroMsg(double* msgBox);
        // check if a point is out side of image range
        bool outOfRange(const SDoublePlane &img, const Point &pt);
        // pairwise cost function
        double pairCost(int i, int j);
        // unary cost function
        double unaryCost(int d, const SDoublePlane &targetImg, int wndSz);
        // given image and point position, return intensity value
        double pixelVal(const SDoublePlane &img, const Point &pt);
};
#endif
