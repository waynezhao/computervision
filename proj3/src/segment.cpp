//----------------------------------------------------------------------------
// segmentation part for proj3, using naive method (which basically makes naive
// bayes assumption) and mrf (markov random filed) 
// I changed data structure for foreground and background pixels from 'vector'
// to 'unordered_set' for faster query 
//-------------
// Reference:
//-------------
//      1)  http://en.highscore.de/cpp/boost/
//          Boost library documentation and examples
//============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Fri,Apr 18th 2014 10:55:31 PM EDT
//          Modified from skeleton code distributed by D.Crandall
// Last Modified: Sun,Apr 20th 2014 08:36:58 PM EDT
//----------------------------------------------------------------------------
#include"SImage.h"
#include"SImageIO.h"
#include<vector>
#include<iostream>
#include<fstream>
#include<map>
#include<math.h>
#include<cstdlib>
#include<boost/unordered_set.hpp> 
#include"split.h"
#include"segMRF.h"
#include"segMRFNode.h"
// for creating dir
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>


int main(int argc, char *argv[]) {
    //-------------------------
    // check command line input
    //-------------------------
    if(argc != 3) {
        std::cerr << "Usage: " <<argv[0]<<" <image_file> <seeds_file>" << endl;
        return 1;
    }
    std::string input_filename1 = argv[1], seeds_file = argv[2];
    // extract name of figure
    unsigned pos = input_filename1.find_last_of("/");
    std::string figName = input_filename1.substr(pos+1);
    pos = figName.find_last_of(".");
    figName = figName.substr(0,pos);

    //--------------------------------------
    // create result dir and sublevel dirs
    //--------------------------------------
    std::string resDir="results";
    std::string segDir=resDir+"/segment";
    std::string imgDir=segDir+"/"+figName;

    //-------------------
    // check if dir exist
    //-------------------
    struct stat st;
    if (stat(resDir.c_str(), &st) == -1) {
        cout<<"creating dir: --> "<<resDir<<endl;
        mkdir(resDir.c_str(), 0755);
    }
    if (stat(segDir.c_str(), &st) == -1) {
        cout<<"creating subdir: --> "<<segDir<<endl;
        mkdir(segDir.c_str(), 0755);
    }
    if (stat(imgDir.c_str(), &st) == -1) {
        cout<<"creating subdir: --> "<<imgDir<<endl;
        mkdir(imgDir.c_str(), 0755);
    }

    //-------------------------------
    // read in image and seed image
    //-------------------------------
    SDoublePlane image_rgb[3], seeds_rgb[3]; 
    SImageIO::read_png_file_rgb(input_filename1.c_str(), image_rgb);
    SImageIO::read_png_file_rgb(seeds_file.c_str(), seeds_rgb);

    //--------------------------
    // figure out seed points 
    //--------------------------
    ptSet fg_pixels, bg_pixels;
    for(int i=0; i<seeds_rgb[0].rows(); i++) {
        for(int j=0; j<seeds_rgb[0].cols(); j++) {
            // blue --> foreground
            if(seeds_rgb[0][i][j] < 100 && seeds_rgb[1][i][j] < 100 && seeds_rgb[2][i][j] > 100)
                fg_pixels.insert(Point(i, j));

            // red --> background
            if(seeds_rgb[0][i][j] > 100 && seeds_rgb[1][i][j] < 100 && seeds_rgb[2][i][j] < 100)
                bg_pixels.insert(Point(i, j));
        }
    }

    //------------------------
    // do naive segmentation
    //------------------------
    SDoublePlane labels = naive_segment(image_rgb, fg_pixels, bg_pixels);
    output_segmentation(image_rgb, labels, imgDir+"/naiveSegment");
    // with modified naive segmentation
    labels = naive_segment_mod(image_rgb, fg_pixels, bg_pixels);
    output_segmentation(image_rgb, labels, imgDir+"/modNaiveSegment");

    //----------------------
    // Do MRF segmentation
    //----------------------
    // learn foreground mean and var from fg point
    vecDoubles fgMean, fgVar;
    getMeanAndVariance(image_rgb, fg_pixels, fgMean, fgVar);
    vecDoubles bgMean, bgVar;
    getMeanAndVariance(image_rgb, bg_pixels, bgMean, bgVar);
    // print variance
    printf(" Foreground Variance: (%.2e, %.2e, %.2e) \n", fgVar[0], fgVar[1], fgVar[2]);
    printf(" Background Variance: (%.2e, %.2e, %.2e) \n", bgVar[0], bgVar[1], bgVar[2]);
    // mrf without modified cost function
    int maxIterations= 100;
    segMRF mrf(image_rgb,2, maxIterations);
    labels = mrf.loopyBP(fg_pixels, bg_pixels, fgMean, fgVar);
    output_segmentation(image_rgb, labels, imgDir+"/mrfSegment");
    // mrf segmentation with modified cost function
    segMRF modMRF(image_rgb, 2, maxIterations);
    labels = modMRF.loopyBP(fg_pixels, bg_pixels, fgMean, fgVar, bgMean, bgVar);
    output_segmentation(image_rgb, labels, imgDir+"/modMRFSegment");
    
    return 0;
}
