/*
 * Copyright 2002-2008 Guillaume Cottenceau.
 *
 * This software may be freely redistributed under the terms
 * of the X11 license.
 *
 */

// Hacked by D. Crandall, 9/2010
//----------------------------------------------------------------------------
// Separate class declaration from implementation and study the implementation
// Weiran Zhao
// Started: Thu,Jan 30th 2014 03:04:24 AM EST
// Modified: Mon,Feb 03th 2014 03:52:44 PM EST
//           Migrate comments about SImage class from omr.cpp
// Modified: Thu,Apr 10th 2014 09:46:00 AM EDT
//           copy read_png_file_rgb() from proj3 skeleton file, from D. Crandall
// Last Modified: Thu,Apr 10th 2014 09:47:08 AM EDT
//----------------------------------------------------------------------------

#ifndef _SIMAGEIO_H_
#define _SIMAGEIO_H_

#include"SImage.h"
#include<vector>
#include<string>
using namespace std;

//----------------------------------------------------------------------------
// The simple image class is called SDoublePlane, with each pixel represented as
// a double (floating point) type. This means that an SDoublePlane can represent
// values outside the range 0-255, and thus can represent squared gradient
// magnitudes, harris corner scores, etc. 
//
// The SImageIO class supports reading and writing PNG files. It will read in a
// color PNG file, convert it to grayscale, and then return it to you in an
// SDoublePlane. The values in this SDoublePlane will be in the range [0,255].
//
// To write out an image, call write_png_file(). It takes three separate planes,
// one for each primary color (red, green, blue). To write a grayscale image,
// just pass the same SDoublePlane for all 3 planes. In order to get sensible
// results, the values in the SDoublePlane should be in the range [0,255].
//----------------------------------------------------------------------------
class SImageIO
{
    protected:
        static void abort_(const char * s, ...);

    public:
        // Below is a helper functions that overlays rectangles
        // on an image plane for visualization purpose. 
        // Draws a rectangle on an image plane, using the specified gray level value and line width.
        static void overlay_rectangle(SDoublePlane &input, int _top, int _left, int _bottom, int _right, double graylevel, int width);

        // read a color image, and convert to a single grayscale plane
        static SDoublePlane read_png_file(const char *file_name);

        // read a color image, into three separate red, green, and blue color planes
        static void read_png_file(const char* file_name,  SDoublePlane &red_plane,  SDoublePlane &green_plane,  SDoublePlane &blue_plane);
        static void write_png_file(const char* file_name, const SDoublePlane &img_r, const SDoublePlane &img_g, const SDoublePlane &img_b);
        // Function that write staffs on image
        static void write_staff_image(const string &filename, const vector<vector<int> > &staveLines, const SDoublePlane &input);
        // read png file in rgb format (copied from skeleton code of proj3)
        static void read_png_file_rgb(const char *filename, SDoublePlane *rgb);
};

#endif
