// ----------------------------------------------------------------------------
// Class for representing MRF network for segmentation problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sat,Apr 19th 2014 09:54:05 PM EDT
// Last Modified: Sun,Apr 20th 2014 02:13:23 PM EDT
// ----------------------------------------------------------------------------
#include"segMRF.h"
#include<cstdlib>
#include<iostream>

segMRF::segMRF(SDoublePlane *image, int num, int maxIterations) {
    this->img = image;
    this->maxIter = maxIterations;
    this->numStates = num;
    // create all nodes
    int rows = img[0].rows(), cols = img[0].cols();
    allNodes = (segMRFNode**) malloc(sizeof(segMRFNode*)*rows);
    for (int i = 0; i < rows; ++i) {
        allNodes[i] = (segMRFNode*) malloc(sizeof(segMRFNode)*cols);
    }
    
    // initialize all nodes
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j] = segMRFNode(Point(i,j), allNodes, numStates, img);
        }
    }
}

segMRF::~segMRF() {
    for (int i = 0; i < img[0].rows(); ++i) {
        free(allNodes[i]);
    }
    free(allNodes);
}

SDoublePlane segMRF::loopyBP( const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar ) {
    //-------------------------------------------
    // let each node set its' evidence messages
    //-------------------------------------------
    int rows = img[0].rows();
    int cols = img[0].cols();
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].getEvidenceMsg(fg,bg,fgMean,fgVar);
        }
    }

    //---------------------------------
    // pass message for maxIter times
    //---------------------------------
    for (int k = 0; k < maxIter; k++) {
        // send messages
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].getAllMsg();
            }
        }
        // update message box
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].updateMsgBox();
            }
        }
        //std::cout << "sending message pass "<<k << std::endl;
        //for(int i=0; i<4; i++) {
        //    printMsg(allNodes[33][45].newPairwiseMsg[i], numStates);
        //}
    }

    //----------------------
    // make final decision
    //----------------------
    SDoublePlane labels(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].decideMAPState();
            labels[i][j] = allNodes[i][j].finalState;
        }
    }

    // return labels
    return labels;
}

SDoublePlane segMRF::loopyBP( const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar, const vecDoubles &bgMean, const vecDoubles &bgVar) {
    //-------------------------------------------
    // let each node set its' evidence messages
    //-------------------------------------------
    int rows = img[0].rows();
    int cols = img[0].cols();
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].getEvidenceMsg(fg,bg,fgMean,fgVar,bgMean, bgVar);
        }
    }

    //---------------------------------
    // pass message for maxIter times
    //---------------------------------
    for (int k = 0; k < maxIter; k++) {
        // send messages
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].getAllMsg();
            }
        }
        // update message box
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].updateMsgBox();
            }
        }
        //std::cout << "sending message pass "<<k << std::endl;
        //for(int i=0; i<4; i++) {
        //    printMsg(allNodes[33][45].newPairwiseMsg[i], numStates);
        //}
    }

    //----------------------
    // make final decision
    //----------------------
    SDoublePlane labels(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].decideMAPState();
            labels[i][j] = allNodes[i][j].finalState;
        }
    }

    // return labels
    return labels;
}
