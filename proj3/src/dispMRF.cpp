// ----------------------------------------------------------------------------
// Class for representing MRF network for stereo problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 20th 2014 05:08:16 PM EDT 
//          Basically copy from dispMRF.h
// Last Modified: Sun,Apr 20th 2014 09:13:17 PM EDT
// ----------------------------------------------------------------------------
#include"dispMRF.h"
#include<cstdlib>
#include<iostream>

dispMRF::dispMRF(SDoublePlane *image, int num, int maxIterations) {
    this->img = image;
    this->maxIter = maxIterations;
    this->numStates = num;
    // create all nodes
    int rows = img[0].rows(), cols = img[0].cols();
    allNodes = (dispMRFNode**) malloc(sizeof(dispMRFNode*)*rows);
    for (int i = 0; i < rows; ++i) {
        allNodes[i] = (dispMRFNode*) malloc(sizeof(dispMRFNode)*cols);
    }
    
    // initialize all nodes
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j] = dispMRFNode(Point(i,j), allNodes, numStates, img);
        }
    }
}

dispMRF::~dispMRF() {
    for (int i = 0; i < img[0].rows(); ++i) {
        free(allNodes[i]);
    }
    free(allNodes);
}

SDoublePlane dispMRF::loopyBP(const SDoublePlane &targetImg, int wndSz) {
    //-------------------------------------------
    // let each node set its' evidence messages
    //-------------------------------------------
    int rows = img[0].rows();
    int cols = img[0].cols();
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].getEvidenceMsg(targetImg, wndSz);
        }
    }

    //---------------------------------
    // pass message for maxIter times
    //---------------------------------
    for (int k = 0; k < maxIter; k++) {
        // send messages
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].getAllMsg();
            }
        }
        // update message box
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                allNodes[i][j].updateMsgBox();
            }
        }
        //std::cout << "sending message pass "<<k << std::endl;
        //for(int i=0; i<4; i++) {
        //    printMsg(allNodes[33][45].newPairwiseMsg[i], numStates);
        //}
    }

    //----------------------
    // make final decision
    //----------------------
    SDoublePlane labels(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            allNodes[i][j].decideMAPState();
            labels[i][j] = allNodes[i][j].finalState;
        }
    }

    // return labels
    return labels;
}

