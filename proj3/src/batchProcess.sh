#!/bin/bash

#segImgs=`find images/segment -name "*.png" ! -name "*-seeds*"`
#echo "============================================================ "
#echo "Segmentation problem "
#for img in $segImgs; do
#    seedImg=${img%.png}-seeds.png
#    echo "------------------------------------------------------------------ "
#    echo "processing image pair: `basename $img` and `basename $seedImg`"
#    echo "------------------------------------------------------------------ "
#    time ./segment $img $seedImg
#    #./segment $img $seedImg
#done

steImgs=`find images/stereo -mindepth 1 -type d`
echo "============================================================ "
echo "Stereo problem, calculate disp for view5, use view1 evidence"
for img in $steImgs; do
    target=$img"/view5.png"
    evidence=$img"/view1.png"
    groundTruth=$img"/disp5.png"
    echo "--------------------------------------------"
    echo "processing images in `basename $img`"
    echo "--------------------------------------------"
    time ./stereo $target $evidence $groundTruth
    #./stereo $target $evidence $groundTruth
done
