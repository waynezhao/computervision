// ----------------------------------------------------------------------------
// Class for representing MRF network for stereo problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 20th 2014 05:08:16 PM EDT 
//          Basically copy from segMRF.h
// Last Modified: Sun,Apr 20th 2014 05:54:49 PM EDT
// ----------------------------------------------------------------------------
#ifndef _DISP_MRF__H__
#define _DISP_MRF__H__ 

#include"dispMRFNode.h"
#include"SImage.h"

class dispMRF{
    public:
        dispMRFNode **allNodes;      // pointer to all MRF nodes
        SDoublePlane *img;          // pointer to original image img[0],img[1],img[2]
        int maxIter;                // maximum number of iteration of LoopyBP
        int numStates;              // number of possible states for hidden node

        //----------
        // methods
        //----------
        // constructor
        dispMRF(SDoublePlane *image, int num, int maxIterations = 100);
        // deconstructor
        ~dispMRF();

        // loopy belief propagation
        SDoublePlane loopyBP(const SDoublePlane &targetImg, int wndSz);
};
#endif
