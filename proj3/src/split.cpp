//----------------------------------------------------------------------------
// This file contains function and datastructure used for image segmentation
// (step 2, 3)
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sat,Apr 19th 2014 02:00:07 AM EDT
// Last Modified: Sun,Apr 20th 2014 04:58:28 PM EDT
// ----------------------------------------------------------------------------
#include"split.h"
#include<cmath>
#include<limits>
#include<iostream>
#include<cstdio>

Point::Point(int _r, int _c) {
    row = _r;
    col = _c;
}


bool Point::operator==(const Point &p) const { 
    return row == p.row && col == p.col; 
} 

bool Point::ptInSet(const boost::unordered_set<Point> & s) {
    return s.find(*this) != s.end();
}

Point::Point(const Point &pt) {
    row = pt.row;
    col = pt.col;
}

Point& Point::operator= (const Point &pt) {
    this->row = pt.row;
    this->col = pt.col;
    return *this;
}

void Point::print() {
    std::cout << "( "<<row<<" , "<<col<<")" << std::endl;
}

std::size_t hash_value(Point const &p) { 
    std::size_t seed = 0; 
    boost::hash_combine(seed, p.row); 
    boost::hash_combine(seed, p.col); 
    return seed; 
} 

void output_segmentation(const SDoublePlane *img, const SDoublePlane &labels, const std::string &fname) {
    //-----------------
    // check parameters
    //-----------------
    assert(img[0].rows() == labels.rows());
    assert(img[0].cols() == labels.cols());

    SDoublePlane img_fg[3], img_bg[3];

    for(int i=0; i<3; i++) {
        img_fg[i] = img[i];
        img_bg[i] = img[i];
    }

    for(int i=0; i<labels.rows(); i++) {
        for(int j=0; j<labels.cols(); j++) {
            //------------------------------------------------------------------
            // when label is "BACK_GROUND", set corresponding foreground image's
            // value to 0; when label "FORE_GROUND", set background image's
            // pixel value to 0.
            //------------------------------------------------------------------
            if(labels[i][j] == BACK_GROUND) {
                img_fg[0][i][j] = img_fg[1][i][j] = img_fg[2][i][j] = 0;
            } else if(labels[i][j] == FORE_GROUND) { 
                img_bg[0][i][j] = img_bg[1][i][j] = img_bg[2][i][j] = 0;
            } else { // code should not run here
                assert(0);
            }
        }
    }

    SImageIO::write_png_file((fname + "FG.png").c_str(), img_fg[0], img_fg[1], img_fg[2]);
    SImageIO::write_png_file((fname + "BG.png").c_str(), img_bg[0], img_bg[1], img_bg[2]);
}

double gaussian(double target, double mean, double var) {
    return 1/sqrt(2*PI*var)*exp(-(target-mean)*(target-mean)/2/var);
}

void getMeanAndVariance(const SDoublePlane *img, const ptSet &pos, vecDoubles &meanSet, vecDoubles &varSet) {
    double mean[3], var[3];
    mean[0] = mean[1] = mean[2] = 0;
    var[0] = var[1] = var[2] = 0;
    double tmpPixel;
    // loop through all position pts
    for(ptSet::iterator it = pos.begin(); it != pos.end(); ++it) {
        // for R G B respectively
        for (int i = 0; i < 3; ++i) {
            tmpPixel = img[i][it->row][it->col];
            mean[i] += tmpPixel;
            var[i] += tmpPixel*tmpPixel;
        }
    }
    // calculate mean and var and push to their corresponding set
    for (int i = 0; i < 3; ++i) {
        mean[i] /= pos.size();
        var[i] /= pos.size();
        var[i] -= mean[i]*mean[i];
        meanSet.push_back(mean[i]);
        varSet.push_back(var[i]);
    }
}

SDoublePlane naive_segment(const SDoublePlane *img, const ptSet &fg, const ptSet &bg) {
    // create label matrix
    SDoublePlane label(img[0].rows(), img[0].cols());
    // create cost matrix for label = background, and label = foreground 
    SDoublePlane costBG(label.rows(), label.cols());
    SDoublePlane costFG(label.rows(), label.cols());

    // learn foreground mean and var from fg point
    vecDoubles fgMean, fgVar;
    getMeanAndVariance(img, fg, fgMean, fgVar);
    
    // calculate unary cost matrix 
    Point ptTmp;
    for (int i = 0; i < costBG.rows(); ++i) {
        for (int j = 0; j < costBG.cols(); ++j) {
            ptTmp = Point(i,j);
            costBG[i][j] = unaryCost(BACK_GROUND, img, ptTmp, fg, bg, fgMean, fgVar);
            costFG[i][j] = unaryCost(FORE_GROUND, img, ptTmp, fg, bg, fgMean, fgVar);
        }
    }

    // make a decision based on costBG and costFG
    for (int i = 0; i < label.rows(); ++i) {
        for (int j = 0; j < label.cols(); ++j) {
            if(costBG[i][j] > costFG[i][j]) {
                label[i][j] = FORE_GROUND;
            } else {
                label[i][j] = BACK_GROUND;
            }
        }
    }

    return label;
}

double unaryCost(int label, const SDoublePlane *img, Point pt, const ptSet &fg, const ptSet &bg, const vecDoubles &mean, const vecDoubles &var) {
    // assign lable to foreground 
    if(label == FORE_GROUND) {
        // we know point in fg
        if(pt.ptInSet(fg)) {
            return 0;
        } else {
            if(pt.ptInSet(bg)) { // we know point in bg;
                return std::numeric_limits<double>::max();
            } else { // we don't know, use prob
                double prob=1;
                for (int i = 0; i < 3; ++i) {
                    prob *= gaussian(img[i][pt.row][pt.col], mean[i], var[i]);
                }
                return -log(prob);
            }
        }
    } else { // label assign to background
        // we know point in fg
        if(pt.ptInSet(fg)) {
            return std::numeric_limits<double>::max();
        } else {
            if(pt.ptInSet(bg)) { // we know pt in bg;
                return 0;
            } else { // use const cost
                return BETA;
            }
        }
    }
}

double unaryCostMod(int label, const SDoublePlane *img, Point pt, const ptSet &fg, const ptSet &bg, const vecDoubles &fgMean, const vecDoubles &fgVar, const vecDoubles &bgMean, const vecDoubles &bgVar){
    // assign lable to foreground 
    if(label == FORE_GROUND) {
        // we know point in fg
        if(pt.ptInSet(fg)) {
            return 0;
        } else {
            if(pt.ptInSet(bg)) { // we know point in bg;
                return std::numeric_limits<double>::max();
            } else { // we don't know, use prob
                double prob=1;
                for (int i = 0; i < 3; ++i) {
                    prob *= gaussian(img[i][pt.row][pt.col], fgMean[i], fgVar[i]);
                }
                return -log(prob);
            }
        }
    } else { // label assign to background
        // we know point in fg
        if(pt.ptInSet(fg)) {
            return std::numeric_limits<double>::max();
        } else {
            if(pt.ptInSet(bg)) { // we know pt in bg;
                return 0;
            } else { // use prob
                double prob=1;
                for (int i = 0; i < 3; ++i) {
                    prob *= gaussian(img[i][pt.row][pt.col], bgMean[i], bgVar[i]);
                }
                return -log(prob);
            }
        }
    }
}

SDoublePlane naive_segment_mod(const SDoublePlane *img, const ptSet &fg, const ptSet &bg) {
    // create label matrix
    SDoublePlane label(img[0].rows(), img[0].cols());
    // create cost matrix for label = background, and label = foreground 
    SDoublePlane costBG(label.rows(), label.cols());
    SDoublePlane costFG(label.rows(), label.cols());

    // learn foreground mean and var from fg point
    vecDoubles fgMean, fgVar;
    getMeanAndVariance(img, fg, fgMean, fgVar);
    vecDoubles bgMean, bgVar;
    getMeanAndVariance(img, bg, bgMean, bgVar);

    // calculate unary cost matrix 
    Point ptTmp;
    for (int i = 0; i < costBG.rows(); ++i) {
        for (int j = 0; j < costBG.cols(); ++j) {
            ptTmp = Point(i,j);
            costBG[i][j] = unaryCostMod(BACK_GROUND, img, ptTmp, fg, bg, fgMean, fgVar, bgMean, bgVar);
            costFG[i][j] = unaryCostMod(FORE_GROUND, img, ptTmp, fg, bg, fgMean, fgVar, bgMean, bgVar);
        }
    }

    // make a decision based on costBG and costFG
    for (int i = 0; i < label.rows(); ++i) {
        for (int j = 0; j < label.cols(); ++j) {
            if(costBG[i][j] > costFG[i][j]) {
                label[i][j] = FORE_GROUND;
            } else {
                label[i][j] = BACK_GROUND;
            }
        }
    }

    return label;
}

void printMsg(double* msg, int len) {
    for (int i = 0; i < len; ++i) {
        printf("(%d, %.6e)\n", i, msg[i]);
    }
}
