// ----------------------------------------------------------------------------
// Class for reprensenting a MRF node for stereo problem
// ============================================================
// Weiran (Ryan) Zhao 
// Computer Science Dept, IU Bloomington
// Started: Sun,Apr 20th 2014 05:10:54 PM EDT
//          Copy from dispMRFNode.h/cpp
// Last Modified: Sun,Apr 20th 2014 10:16:59 PM EDT
// ----------------------------------------------------------------------------
#include"dispMRFNode.h"
#include<algorithm>
#include<iostream>
#include<cstdlib>
#include<cstring>

dispMRFNode::dispMRFNode(Point pos, dispMRFNode **allNodes, int numStates, SDoublePlane *img) {
    this->myPos = pos;
    this->allNodes = allNodes;
    this->numStates = numStates;
    this->img = img;
    // create msg box for evidence
    evidenceMsg = (double*) malloc(sizeof(double)*numStates);
    // msg box for pairwise messages
    this->oldPairwiseMsg= (double**) malloc(sizeof(double*)*4);
    this->newPairwiseMsg = (double**) malloc(sizeof(double*)*4);
    for (int i = 0; i < 4; ++i) {
        this->oldPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
        this->newPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
    }
    // initialize pairwise messages
    for (int i = 0; i < 4; ++i) {
        zeroMsg(this->oldPairwiseMsg[i]);
        zeroMsg(this->oldPairwiseMsg[i]);
    }
    zeroMsg(evidenceMsg);
}

dispMRFNode::dispMRFNode(const dispMRFNode &nd) {
    this->numStates = nd.numStates;
    //---------------------------------
    // for pointers we need copy deeper
    //---------------------------------
    this->evidenceMsg = (double*) malloc(sizeof(double)*numStates);
    memcpy(this->evidenceMsg, nd.evidenceMsg, sizeof(double)*numStates);
    this->oldPairwiseMsg = (double**) malloc(sizeof(double*)*4);
    this->newPairwiseMsg = (double**) malloc(sizeof(double*)*4);
    for (int i = 0; i < 4; ++i) {
        this->oldPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
        memcpy(this->oldPairwiseMsg[i], nd.oldPairwiseMsg[i], sizeof(double)*numStates);
        this->newPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
        memcpy(this->newPairwiseMsg[i], nd.newPairwiseMsg[i], sizeof(double)*numStates);
    }
    this->finalState = nd.finalState;
    this->myPos = nd.myPos;
    this->allNodes = nd.allNodes;
    this->img = nd.img;
}

dispMRFNode& dispMRFNode::operator=( const dispMRFNode &nd) {
    this->numStates = nd.numStates;
    //---------------------------------
    // for pointers we need copy deeper
    //---------------------------------
    this->evidenceMsg = (double*) malloc(sizeof(double)*numStates);
    memcpy(this->evidenceMsg, nd.evidenceMsg, sizeof(double)*numStates);
    this->oldPairwiseMsg = (double**) malloc(sizeof(double*)*4);
    this->newPairwiseMsg = (double**) malloc(sizeof(double*)*4);
    for (int i = 0; i < 4; ++i) {
        this->oldPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
        memcpy(this->oldPairwiseMsg[i], nd.oldPairwiseMsg[i], sizeof(double)*numStates);
        this->newPairwiseMsg[i] = (double*) malloc(sizeof(double)*numStates);
        memcpy(this->newPairwiseMsg[i], nd.newPairwiseMsg[i], sizeof(double)*numStates);
    }
    this->finalState = nd.finalState;
    this->myPos = nd.myPos;
    this->allNodes = nd.allNodes;
    this->img = nd.img;
    return *this;
}

dispMRFNode::~dispMRFNode() {
    // free memory
    free(evidenceMsg);
    for (int i = 0; i < 4; ++i) {
        free(oldPairwiseMsg[i]);
        free(newPairwiseMsg[i]);
    }
    free(oldPairwiseMsg);
    free(newPairwiseMsg);
}

void dispMRFNode::zeroMsg(double* msgBox) {
    for (int i = 0; i < numStates; ++i) {
        msgBox[i] = 0;
    }
}

void dispMRFNode::getEvidenceMsg(const SDoublePlane &targetImg, int wndSz) {
    for (int i = 0; i < numStates; ++i) {
        // this is where we should call unary cost function
        this->evidenceMsg[i] = unaryCost(i, targetImg, wndSz);
    }
}

void dispMRFNode::getMsgFrom(int dir) {
    // find the node that I should receive message from
    Point target = myPos;
    switch(dir) {
        case UP:
            target.row -= 1;
            break;
        case RIGHT:
            target.col += 1;
            break;
        case DOWN:
            target.row += 1;
            break;
        case LEFT:
            target.col -= 1;
            break;
        default:
            assert(0==1);
            break;
    }
    // check if target is out of range
    if( outOfRange(*img, target)) {
        zeroMsg(newPairwiseMsg[dir]);
    } else { // receive message from that node
        allNodes[target.row][target.col].sendMsgTo((dir+2)%4, newPairwiseMsg[dir]);
    }
}

//void dispMRFNode::sendMsgTo(int dir, double* msgBox) {
//    // note that loop index is inverse of normally used, just to conform with
//    // formula of lecture 16
//    double minCost, tmp, mean=0; 
//    for(int j=0;j<numStates;++j) {
//        minCost = std::numeric_limits<double>::max();
//        for(int i=0;i<numStates;++i) {
//            tmp = 0;
//            // evidence cost
//            tmp += this->evidenceMsg[i];
//            // pairwise cost
//            tmp += this->pairCost(i,j);
//            // my local message
//            for(int k=0; k<4; ++k) {
//                // make sure I am using message from that node
//                if(k != dir) {
//                    tmp += this->oldPairwiseMsg[k][i];
//                }
//            }
//            // update minCost
//            minCost = std::min(tmp, minCost);
//        }
//        msgBox[j] = minCost;
//        mean += msgBox[j];
//    }
//    mean /= numStates;
//    //---------------------------
//    // send normalized messages
//    //---------------------------
//    for (int i = 0; i < numStates; ++i) {
//        msgBox[i] -= mean;
//    }
//}

//-----------------------------------------------------------------------------
// this function assuming using a linear cost model and will try to boost the
// processing time to O(n) using technique of lecture 17 (find lower envelope)
//-----------------------------------------------------------------------------
void dispMRFNode::sendMsgTo(int dir, double* msgBox) {
    //---------------------------------------
    // compute msgBox should be O(numStates)
    //---------------------------------------
    for (int i = 0; i < numStates; ++i) {
        msgBox[i] =0;
        msgBox[i] += this->evidenceMsg[i];
        for(int j=0; j < 4; ++j) {
            // not send back message it receives
            if(j!=dir) {
                msgBox[i] +=this->oldPairwiseMsg[j][i];
            }
        }
    }

    //---------------
    // forward pass
    //---------------
    for (int i = 1; i < numStates; ++i) {
        msgBox[i] = std::min(msgBox[i], msgBox[i-1]+ALPHA);
    }

    //----------------
    // backward pass
    //----------------
    double mean = msgBox[numStates-1];
    for (int i = numStates-2; i>=0; --i) {
        msgBox[i] = std::min(msgBox[i], msgBox[i+1]+ALPHA);
        mean += msgBox[i];
    }

    //----------------------------
    // send "normalized" message
    //----------------------------
    mean /= numStates;
    for (int i = 0; i < numStates; ++i) {
        msgBox[i] -= mean;
    }
}

bool dispMRFNode::outOfRange(const SDoublePlane &img, const Point &pt) {
    if( (pt.row < img.rows()) &&
        (pt.row >= 0) &&
        (pt.col < img.cols()) &&
        (pt.col >=0 ) ) {
        return false;
    } else {
        return true;
    }
}

double dispMRFNode::pairCost(int i, int j) {
    return (i-j)*(i-j)*ALPHA;
}

void dispMRFNode::decideMAPState() {
    double minCost = std::numeric_limits<double>::max();
    double tmp;
    int minIdx = 0;
    for(int j=0; j<numStates; ++j) {
        tmp = 0;
        tmp += this->evidenceMsg[j];
        // add cost from 4 messages
        for(int i=0; i<4; ++i) {
            tmp += this->newPairwiseMsg[i][j];
        }
        // decide if update minCost
        if(tmp < minCost) {
            minCost = tmp;
            minIdx = j;
        }
    }
    finalState = minIdx;
}

void dispMRFNode::getAllMsg() {
    // get message from 4 directions
    for (int i = 0; i < 4; ++i) {
        this->getMsgFrom(i);
    }
}

void dispMRFNode::updateMsgBox() {
    // update oldPairwiseMsg with newPairwiseMsg
    for (int i = 0; i < 4; ++i) {
        memcpy(this->oldPairwiseMsg[i], this->newPairwiseMsg[i], sizeof(double)*numStates);
    }
}

double dispMRFNode::unaryCost(int d, const SDoublePlane &targetImg, int wndSz) {
    double cost = 0.0;
    double myVal, targetVal;
    Point tmpPt;
    // loop through this small window
    for(int u=-wndSz; u<=wndSz; u++) {
        for (int v = -wndSz; v <= wndSz; v++) {
            tmpPt = Point(myPos.row+u, myPos.col+v);
            myVal = pixelVal(*img, tmpPt);
            //----------------------------------
            // Formula for left to right image
            //----------------------------------
            tmpPt.col += d;
            //----------------------------------
            // Formula for right to left image
            //----------------------------------
            //tmpPt.col -= d;
            targetVal = pixelVal(targetImg, tmpPt);
            cost += (myVal-targetVal) * (myVal-targetVal);
        }
    }

    return cost;
}

double dispMRFNode::pixelVal(const SDoublePlane &img, const Point &pt) {
    // out of range pixel value is 0
    if(outOfRange(img, pt)) {
        return 0;
    } else {
        return img[pt.row][pt.col];
    }
}
