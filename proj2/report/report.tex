\documentclass[12pt]{article}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{amsmath}
%http://tex.stackexchange.com/questions/58713/ref-should-use-enumerate-label-name
\usepackage{enumitem}
\usepackage{graphicx}
\setcounter{section}{-1}
\usepackage{color}
\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\begin{document}

\title{Computer Vision\\Feature Detection and Projective Transforms\\Assignment 2 Report}
\author{Weiran Zhao\\zhaoweir@indiana.edu}
\date{Sunday, Mar 9th, 2014}
\maketitle

\newpage
\tableofcontents

\newpage
\listoffigures

\newpage
\section{How to run the code}
Code has been tested under ``\verb|burrow.cs.indiana.edu|''. \\
I will assume the name of image sequence need to be processed is ``\verb|seq3|'' for the following steps.
\subsection{Run program}
\begin{enumerate}
\item Run ``\verb|tar -zxvf code_zhaoweir.tgz|'' to decompress the tar file, and a folder called ``\verb|src|'' should be created
\item Dump ``\verb|seq3|'' to ``\verb|src|'' folder
\item Run ``\verb|./prepare seq3|'' to convert ``jpg'' or ``jpeg'' images to ``png'' format; also generated is a file called ``\verb|imageNames_seq3.txt|'' under ``\verb|src|'' folder
\item If you want a particular image to be used as the ``base perspective'', put it at the second line of ``\verb|imageNames_seq3.txt|''; otherwise, the ``base perspective'' image is the one whose name rank first
\item Run ``\verb|make|'' (``\verb|make kleen|'' might be needed to clean previous results) 
\item Type ``\verb|./pvt imageNames_seq3.txt|'' to let the program run
\end{enumerate}
\subsection{Program output}
Output of the program consists of several things:
\begin{enumerate}
\item Minor terminal output helps you monitor the process of the program
\item A folder called ``\verb|./results/imageNames_seq3.txt/|'' will be created
\item For each image (assuming ``\verb|orgImgName.png|'') in ``\verb|seq3|'', an image called ``\verb|orgImgName_corners.png|'' and ``\verb|orgImgName_projected.png|'' will be created (except for the base image, only ``\verb|orgImgName_corners.png|'' created)
\end{enumerate}
\subsection{Folder structure}
Now, I will list the meaning of files that's most related to this project.
\begin{enumerate}
\item Harris Corner Detector: ``\verb|harris.h|'' and ``\verb|harris.cpp|''
\item Feature Vector Extraction: ``\verb|featureVec.h|'' and ``\verb|featureVec.cpp|''
\item Projective Transformation: ``\verb|projTrans.h|'' and ``\verb|projTrans.cpp|''
\item Feature Matching, RANSAC: ``\verb|ransac.h|'' and ``\verb|ransac.cpp|''
\end{enumerate}
\section{Introduction} 
In this project, the goal is to write a program that transforms a set of photos, which were of the same scene, but taken from different viewpoints by different people across the span of many month, into pictures that appear to be taken from the same viewpoint. 
\section{Methodology}
In order to fulfill the goal, a set of techniques need to be utilized. The broad line is:
\begin{enumerate}
\item Use (Harris) corner detector to detect corners from each of the images
\item For each corner of an image, extract a feature descriptor, forming a list of feature descriptors that associate with one image
\item Given two lists of feature descriptors associated with two images, match corresponding corners, getting a list of matched corners
\item Using RANSAC (RANdom SAmple Consensus) to select the ``best 4 matched corner'' from matched corners list
\item Use four points method to compute projective transformation between two images
\item Project one image into another image's point of view based on the projective transformation
\end{enumerate}
\subsection{Harris corner detector}
According to \cite{lecNotes}, \cite{harrisMatlab} and \cite{psuHarris}, basic steps for Harris corner detector can be summarized as:
\begin{enumerate}[label=(\alph*)]
\item \label{hars1} Smooth image slightly
\item \label{hars2} Compute derivatives
\item \label{hars3} Compute matrix $H$
\item \label{hars4} Compute Corner Response function for each pixel (\cite{psuHarris})
\item \label{hars5} Compute Non maximal suppression and thresholding
\end{enumerate}
{\color{red} \bf Implementation decisions:}
\begin{enumerate}
\item Step \ref{hars1} and \ref{hars2} can be combined together as one step, if window size for smoothing is 3, then it is a ($3-by-3$) Sobel operator. But for tuning parameters, I did it in two steps. 
\item Also, \cite{sepGssn} confirms that Gaussian 2D kernel is separable, can be done using two 1D passes.
\item \cite{lecNotes} suggests that for Step \ref{hars2} we compute derivative along $45^\circ$ rotated axis; but according to \cite{wikiImgGrad}, $[-1, 0, 1]$ is more commonly used. I used the latter one for its compatibility with existing convolution functions
\item \cite{psuHarris} suggests for Corner Response function 
\begin{equation*}
R(\lambda_{min}, \lambda_{max}) = det(H) - k~trace^2(H)
\end{equation*}
A good experimental constant value for $k$ is 0.04 to 0.06.
\item As for non maximal suppression, I just check each, for each pixel, if it is the max value of a small window. If not, set the value to `0'
\item {\bf I purposely set the threshold for Corner value very high to reduce the number of corner points outputed}
\end{enumerate}

\subsection{Corner feature extraction}
The basic idea of corner feature extraction is to find an ideal representation of a corner that will be invariant across different images. Here is basically what I did:
\begin{enumerate}
\item For each detected corner
\item Create UpLeft, UpRight, DownLeft, DownRight, four square matrices
\item Calculate the gradient strength and gradient orientation for each pixel of the four matrices
\item For each pixel of a matrix, if its gradient strength is greater than some threshold, let the corresponding orientation vote for a certain bin that contains the orientation value
\end{enumerate}
\subsection{Feature point matching}
Following from \cite{lecNotes}, I used the following scheme:
\begin{enumerate}
\item For each feature descriptor ($f^\prime$) in image $A^\prime$
\item Find the closest ($f_1$) and second closest ($f_2$) feature descriptor in image $A$
\item If $\frac{dist(f^\prime, f_1)}{dist(f^\prime, f_2)} < threshold$, accept $(f^\prime, f_1)$ as a correspondence.
\end{enumerate}

\subsection{RANSAC}
\begin{enumerate}
\item Run this method until a certain maximum iteration is reached
\item Within each loop, randomly select four matched points
\item Use four points method to propose a hypothesis $H$
\item Use all matched points to calculate how many of them support this hypothesis
\item Output the most agreed hypothesis $H^*$
\end{enumerate}
\subsection{Compute projection matrix}
This step is quite straight forward, according to \cite{compProj}, just construct the $8-by-8$ matrix using four matched points and solved the corresponding equations. However, extensive tests were done to ensure the correctness of this function.
\begin{enumerate}
\item A set of four matched corners were randomly generated ($myPt_i, targetPt_i, i=1,\dots, 4$) 
\item Use ``fourPtMethod()'' to calculate the projection matrix ($T$)
\item Calculate $estTargetPt_i = T*myPt_i, i=1,\dots,4$
\item Compare if $estTargetPt_i$ and $targetPt_i$ are very close
\end{enumerate}
{\color{red} \bf Interesting result:} If random number generator generates value from 0 to 1,000,000, whose practical meaning is the picture is huge, needs index till 1,000,000. Then it is very likely ($95\%$) to get a wrong projection matrix. This projection matrix is ill conditioned, my guess the $8-by-8$ matrix is also ill conditioned. However, for image size within $5000 \times 5000$, it is OK to use this method.
\subsection{Project an image}
The steps are:
\begin{enumerate}[label=(\alph*)]
\item \label{projImg1} Solve $(x,y,w)^T$ from the following equation:
\begin{equation*}
\left( 
\begin{matrix} 
x^\prime\\  
y^\prime\\
1
\end{matrix} 
\right) = T*
\left( 
\begin{matrix} 
x\\  
y\\
w
\end{matrix} 
\right)
\end{equation*}
\item \label{projImg2} Use bilinear interpolation to calculate the intensity value of image around point $(\frac{x}{w}, \frac{y}{w})$
\end{enumerate}
{\color{red} \bf Implementation pitfalls and decisions:}
\begin{enumerate}
\item Step \ref{projImg1} of above, I call ``LAPACK'' $dgesv$ to solve the equation
\item Step \ref{projImg2} of above, do remember to divide by $w$
\end{enumerate}
\section{Experiment result}
\subsection{Harris}
I used a checker board (Figure~\ref{ckbd}) to test my result (Figure~\ref{ckbdrslt}), and then run the detector on ``lincoln.png'' (Result on Figure~\ref{lclnrslt}, {\bf threshold and window size was set quite high, so number of detected corners are less.})
\begin{figure}[H]
\centering
\caption{Harris corner detector on checker board image}
\label{figChecker}
%original
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{board.png}
\caption{Original image}
\label{ckbd}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{board_harris.png}
\caption{Detected corners}
\label{ckbdrslt}
\end{subfigure}
\end{figure}

% another figure lincoln
\begin{figure}[H]
\centering
\includegraphics[scale=.4]{lincoln_harris.png}
\caption{Detected corners on ``lincoln'' image}
\label{lclnrslt}
\end{figure}
\subsection{Projected Image}
Based on given transformation matrix
\begin{equation*}
\left(
\begin{matrix}
0.907 & 0.258 & -182 \\
-0.153 & 1.44 & 58 \\
-0.000306 & 0.000731 & 1
\end{matrix}
\right)
\end{equation*}
Figure~\ref{lclnproj} shows the result of projected image
% figure projected lincoln
\begin{figure}[H]
\centering
\includegraphics[scale=.4]{lincoln_proj.png}
\caption{Projected ``lincoln'' image based on given transformation matrix}
\label{lclnproj}
\end{figure}

\subsection{Overal test}
\subsubsection{Simple overall test}
The test is just to make sure my program is working when putting different modules together. I will project Figure~\ref{figSimTestMy} back to Figure~\ref{figSimTestTarget} perspective. \\
The result is shown at Figure~\ref{figSimTestRst} (I painted background of Figure~\ref{figSimTestRstGot} black on purpose for better illustration)
\begin{figure}[H]
\centering
\label{figSimTest}
\caption{Project My image back to Target image's perspective}
%original
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{lincoln_proj.png}
\caption{My image}
\label{figSimTestMy}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{lincoln.png}
\caption{Target image}
\label{figSimTestTarget}
\end{subfigure}
\end{figure}
%result
\begin{figure}[H]
\centering
\caption{What I wanted and what I got for projected ``lincoln'' image}
\label{figSimTestRst}
%original
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{lincoln_proj_proj.png}
\caption{What I got}
\label{figSimTestRstGot}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
\includegraphics[width=\textwidth]{lincoln.png}
\caption{What I wanted}
\label{figSimTestRstWanted}
\end{subfigure}
\end{figure}

\subsubsection{Result on test image sequence}
Unfortunately, I have not been able to figure out parameters that will work well for both image sequences. 

I will only list the {\bf successful transformed images} here, others can be found in ``\verb|src/results|'' folder. You will notice my program works OK with ``\verb|seq1|'', but poor with ``\verb|seq2|''. 

Also, you will notice I have a lot of detected corners on the images, that's because I want as many corners as possible, therefore threshold and window size were set relatively small. As for why I am doing this, it's the only way I found can get me more legible images.
\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris1_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris1_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris2_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris2_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris3_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris3_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris4_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris4_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris5_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris5_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris6_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{paris6_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln1_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln1_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln2_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln2_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
%original
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln0_org.png}
\caption{Perspective I wanted}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln3_org.png}
\caption{Image processed}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\textwidth}
\includegraphics[width=\textwidth]{lincoln3_got.png}
\caption{Image I got}
\end{subfigure}
\end{figure}


\section{Conclusion}
This is the case when every module is doing their work OK, but after piecing them together, the system breaks down. 

The key to finding a reasonable projective transformation is to 1) maximize the number of correctly matched feature points and 2) minimized the number of mismatched feature points. 

Indeed RANSAC can deal with mismatched feature points fairly well, however the unsatisfactory result of this program comes from the inability to describe scaled corner and dramatically changed image intensity around corner. I came to this conclusion when I tuned the parameters, if I set larger window size for corner detection, some images worked well; if I change the window size, some other images worked well. Therefore, I don't have enough feature points if I set the criteria high, however, if set low, the quality of matched points degrade. 

I believe If SIFT method or Harris Laplacian method was used, result should be much better. 

Another thing to notice is that ``\verb|seq2|'' is much harder to work with, 1) pictures were take from different angles (facing, left, or right), while ``\verb|seq1|'' is more of facing direction; 2) the building has a lot more repetitive features (columns by columns) than the one in ``\verb|seq1|''. 
\newpage
\begin{thebibliography}{1}
\bibitem{lecNotes} {\em Lecture notes} David Crandall
\bibitem{harrisMatlab} {\em \url{http://denewiler.us/trac/cse252b/browser/hw4/matlab/harris.m}} Matlab version's Harris corner detector
\bibitem{psuHarris} {\em \url{http://www.cse.psu.edu/~rcollins/CSE486/lecture06.pdf}} Harris corner detector notes from PSU
\bibitem{wikiImgGrad} {\em \url{http://en.wikipedia.org/wiki/Image_gradient}} Image Gradient
\bibitem{sepGssn} {\em \url{http://www.stat.wisc.edu/~mchung/teaching/MIA/reading/diffusion.gaussian.kernel.pdf}} The Gaussian Kernel
\bibitem{compProj} {\em \url{http://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/EPSRC_SSAZ/node11.html}} Formula on how to compute projection matrix using 4 points
\end{thebibliography}
\end{document}