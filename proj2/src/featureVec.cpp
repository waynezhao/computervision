//----------------------------------------------------------------------------
// Implementation of feature descriptor extraction
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Mar 06th 2014 01:53:17 PM EST
// Last Modified: Fri,Mar 07th 2014 01:46:18 AM EST
//----------------------------------------------------------------------------
#include"featureVec.h"
#include"magicNum.h"
#include"filter.h"
#include"convolve.h"
#include<algorithm>
#include<cmath>

vector<Descriptor> getFeatureVec(const SDoublePlane &img, vector<Coordinate> &pts) {
    vector<Descriptor> result;
    //----------------------------------------------
    // for each corner pixel, extract feature vector
    //----------------------------------------------
    SDoublePlane corMat, gradX, gradY, orient, magnitude;
    Coordinate point;
    Descriptor descrip;
    // binWidth*DESCRIP_BIN_SZ=2*PI
    double binWidth = 2*MY_PI/DESCRIP_BIN_SZ;
    int idx;
    for(int i=0;i<pts.size();i++) {
        point = pts[i];
        // clear descrip before using
        descrip.clear();
        //--------------------------
        // for each of the 4 corners
        //--------------------------
        for(int j=0;j<4;j++) {
            // construct 8-by-8 matrices according to position 'j'
            corMat = getCornerMat(img, point, j);
            // get gradient of X and Y (X--> column, Y--> row)
            gradX = convolve::convolve_general(corMat, filter::diff_col());
            gradY = convolve::convolve_general(corMat, filter::diff_row());
            // get gradient orientation
            orient = gradX.eleWiseOpsWith(gradY, arcTan);
            // get gradient magnitude
            magnitude = gradX.eleWiseOpsWith(gradY, square);
            //------------------------------------------------------------------
            // threshold based on magnitude and put counts in bins (currently 8)
            //------------------------------------------------------------------
            for(int m=0;m<CORNER_MAT_SZ;m++) {
                for(int n=0;n<CORNER_MAT_SZ;n++) {
                    if(magnitude[m][n]>DESCRIP_EXTRACT_THRESH) {
                        idx = int(floor(orient[m][n]/binWidth));
                        descrip.vector[idx+j*DESCRIP_BIN_SZ]++;
                    }
                }
            }
        }
        //------------------------------------------
        // push back feature points into result list
        //------------------------------------------
        descrip.pt=point;
        result.push_back(descrip);
    }
    return result;
}

SDoublePlane getCornerMat(const SDoublePlane &img, Coordinate point, int pos) {
    // delta_row is the row change, delta_col is the column change
    // needed to find where to start and end to copy pixel value from "img"
    int delta_row, delta_col;
    switch(pos) {
        // upleft
        case 0:
            delta_row = -CORNER_MAT_SZ;
            delta_col = -CORNER_MAT_SZ;
            break;
        // upright
        case 1:
            delta_row = -CORNER_MAT_SZ;
            delta_col = CORNER_MAT_SZ;
            break;
        // downleft
        case 2:
            delta_row = CORNER_MAT_SZ;
            delta_col = -CORNER_MAT_SZ;
            break;
        // downright
        case 3:
            delta_row = CORNER_MAT_SZ;
            delta_col = CORNER_MAT_SZ;
            break;
        default:
            assert(pos<4);
            assert(pos>=0);
            break;
    }

    // declare corner matrix
    // default matrix value is '0's
    SDoublePlane cornerMat(CORNER_MAT_SZ, CORNER_MAT_SZ);

    // locate where to copy pixel value from "img"
    int row_begin = min(point.row+delta_row, point.row);
    int col_begin = min(point.col+delta_col, point.col);

    // img's # of row, and col
    int img_row = img.rows();
    int img_col = img.cols();

    // start copying
    int tmp_row, tmp_col;
    for(int i=0;i<CORNER_MAT_SZ;i++) {
        for(int j=0;j<CORNER_MAT_SZ;j++) {
            // make sure required pixel is within img's range
            tmp_row = row_begin+i;
            tmp_col = col_begin+j;
            if( (tmp_row>=0) && (tmp_row<img_row) && (tmp_col>=0) && (tmp_col<img_col)) {
                cornerMat[i][j] = img[tmp_row][tmp_col];
            } else {
                cornerMat[i][j] = 0;
            }
        }
    }

    return cornerMat;
}

//----------------------------------------------------------------------------
// Helper function implementation
//----------------------------------------------------------------------------
double arcTan(double op1, double op2) {
    // both op1 and op2 are 0
    if( (op1==0) && (op2==0)) {
        return 0.0;
    }
    // op2 need to be y, op1 need to be x
    double res=atan2(op2,op1);
    if(res < 0) {
        res+=2*MY_PI;
    } 
    return res;
}

double square(double op1, double op2) {
    return op1*op1+op2*op2;
}
