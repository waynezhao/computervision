//----------------------------------------------------------------------------
// Using ransac to find a homography between 2 images
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Sat,Mar 08th 2014 01:42:31 PM EST
// Last Modified: Sun,Mar 09th 2014 04:33:11 PM EDT
//----------------------------------------------------------------------------
#ifndef _RANSAC__H__
#define _RANSAC__H__
#include"SImage.h"
#include"newStructs.h"
#include<vector>

using namespace std;

// compute homography using ransac given 2 feature vector lists
vector<MatchedPoint> ransacHomography(const vector<MatchedPoint> matchedPt);

//----------------------------------------------------------------------------
// Helper functions
//----------------------------------------------------------------------------
// given 2 list of features, match them point to point
// using better approach given on Lec 07 (Feb/05/14)
vector<MatchedPoint> featureMatching(vector<Descriptor> targetList, vector<Descriptor> myList);

// given 4 matched point from 2 images, compute projective transfromation matrix
SDoublePlane fourPtMethod(vector<MatchedPoint> matchedPt);

// calculate the distance between 2 points
double distSq(const Coordinate &p1, const Coordinate &p2);

// distance between 2 int vectors
int distSq(const int* vec1, const int* vec2, const int size);

// given a homography, count the number of points that satisfy this matrix
int vote(SDoublePlane homoMat, const vector<MatchedPoint> matchedList);

#endif
