//----------------------------------------------------------------------------
// This file contains routines for doing harris corner detection (declaration)
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Feb 26th 2014 09:31:16 PM EST
// Last Modified: Thu,Mar 06th 2014 12:54:50 AM EST
//----------------------------------------------------------------------------
#ifndef _HARRIS__H__
#define _HARRIS__H__

#include<vector>
#include"SImageIO.h"
#include"newStructs.h"

//----------------------------------------------------------------
// Given an image, use Harris corner detector to find all corners
// return a list of coordinates indicating the position of corners
//----------------------------------------------------------------
vector<Coordinate> harrisCorners(const SDoublePlane &input);

//------------------------------------------------------
// give an binarized image, locate all the corner pixels
//------------------------------------------------------
vector<Coordinate> cornerPixels(const SDoublePlane &input);

#endif
