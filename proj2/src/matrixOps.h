//----------------------------------------------------------------------------
// Some basic matrix operations by calling "Fortran Blas" interface
// 
//----------
// Reference
//----------
//      [1] http://www.cs.indiana.edu/classes/p573/notes/arch/09_blas2.html
//          How to call fortran function in C/C++
//      [2] http://www.netlib.org/lapack/explore-html/d8/d72/dgesv_8f.html
//          LAPack reference 
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Fri,Mar 07th 2014 02:08:55 PM EST
// Last Modified: Sat,Mar 08th 2014 07:33:59 PM EST
//----------------------------------------------------------------------------
extern "C" {
    // solve a general equation A*X=B
    // A is square N-by-N matrix
    extern void dgesv_(int *N,              // order of A [in] 
                       int *nrhs,           // number of column of B [in] 
                       double * Aarray,     // matrix containing A (a lda*N matrix) [in/out]
                       int *lda,            // leading dimension of Aarray [in]
                       int *ipiv,           // pivoting vector for P in A=P*L*U [out]
                       double *Barray,      // matrix B (a ldb*nrhs matrix) [in/out]
                       int *ldb,            // leading dimension of Barray [i]
                       int *info);          // 0 successful, -i argument wrong, i U(i,i)=0 [out]

    // matrix vector multiplication
    extern void dgemv_(char* trans,         // do y=A'x+b [in]
                       int* M,              // # of rows of A [in]
                       int* N,              // # of cols of A [in]
                       double* alpha,       // y=alpha*A'x+beta*b [in]
                       double* Aarray,      // array containing A [in]
                       int* lda,            // leading dimension of A [in]
                       double* X,           // vector x [in]
                       int* incx,           // increment of x [in]
                       double* beta,        // beta [in]
                       double* Y,           // output y [out]
                       int* incy);          // increment of y
};

// solve A*x=b, a wrapper for dgesv, only deals with vector x, and b
// A is guaranteed n-by-n
// BE AWARE: c/c++ are row major, A has been transposed internally, since b is a
// vector, no need to worry about its transpose
void solveEq(int n, const double* A, double* b);

// calculate b=A*x, assuming user allocate appropriate memory
void matVecMul(const double* A, int rowA, int colA, double* x, double* b);
