//----------------------------------------------------------------------------
// Projective transformation Implementation
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Fri,Mar 07th 2014 12:47:03 PM EST
// Last Modified: Sat,Mar 08th 2014 03:38:24 PM EST
//----------------------------------------------------------------------------
#include"projTrans.h"
#include"matrixOps.h"
#include<cstdio>
#include<cstdlib>
#include<cmath>

SDoublePlane projectiveTransformation(const SDoublePlane &img, SDoublePlane &pMat, double ghost) {
    // forcing warped image has the same dimension as given
    SDoublePlane output(img.rows(), img.cols());

    //------------------------------------------
    // loop through each element of output image
    //------------------------------------------
    double orgCoord[3];
    // needed for bilinear interpolation (notation following Lec05 Jan/29/14)
    double a,b,f00,f01,f10,f11,f;
    int lowCol,lowRow,highCol,highRow;
    int numRow=img.rows();
    int numCol=img.cols();

//#define SPEWRESULT
#ifdef SPEWRESULT
        FILE* filex=fopen("x","w");
        FILE* fileb=fopen("b","w");
        FILE* fileA=fopen("A","w");
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                fprintf(fileA,"%d, %d, %.16e\n",i+1, j+1, pMat[j][i]);
            }
        }
#endif
    for(int i=0;i<numRow;i++) {
        for(int j=0;j<numCol;j++) {
            //---------------------------------------------------------
            // calculate corresponding pixel location in original image
            //---------------------------------------------------------
            // homogeneous coordinates CAREFUL about the order of i, j --> 0, 1
            orgCoord[0]=j; orgCoord[1]=i; orgCoord[2]=1;
#ifdef SPEWRESULT
            fprintf(fileb,"%.16e, %.16e, %.16e\n", orgCoord[0], orgCoord[1], orgCoord[2]);
#endif
            solveEq(3,pMat.data_ptr(), orgCoord);
            // NORMALIZE to get the correct coordinates
            orgCoord[0]=orgCoord[0]/orgCoord[2];
            orgCoord[1]=orgCoord[1]/orgCoord[2];
#ifdef SPEWRESULT
            fprintf(filex,"%.16e, %.16e, %.16e\n", orgCoord[0], orgCoord[1], orgCoord[2]);
#endif

            //--------------------------------------------------------
            // use bilinear interpolation to estimate our target pixel
            //--------------------------------------------------------
            lowCol = int(floor(orgCoord[0]));
            lowRow = int(floor(orgCoord[1]));
            highCol = lowCol+1;
            highRow = lowRow+1;
            a = orgCoord[1] - lowRow;
            b = orgCoord[0] - lowCol;
            f00 = ((lowCol>=0) && (lowCol<numCol) && (lowRow>=0) && (lowRow<numRow))? img[lowRow][lowCol]:ghost;
            f01 = ((highCol>=0) && (highCol<numCol) && (lowRow>=0) && (lowRow<numRow))? img[lowRow][highCol]:ghost;
            f10 = ((lowCol>=0) && (lowCol<numCol) && (highRow>=0) && (highRow<numRow))? img[highRow][lowCol]:ghost;
            f11 = ((highCol>=0) && (highCol<numCol) && (highRow>=0) && (highRow<numRow))? img[highRow][highCol]:ghost;
            f = (1-b)*(1-a)*f00+(1-b)*a*f10+b*(1-a)*f01+b*a*f11;
            
            //-------------------
            // put data in output
            //-------------------
            output[i][j] = f;
#ifdef SPEWRESULT
            if( i>670 && j>1014) {
                printf("-------------------------\n");
                printf("ghost element value: %2.2f\n",ghost);
                printf("Coordinates in original image: (%2.2f, %2.2f, %2.2f)\n", orgCoord[0], orgCoord[1], orgCoord[2]);
                printf("Low: (%d, %d); High: (%d, %d)\n", lowCol, lowRow, highCol, highRow);
                printf("f00: %2.2f, f01 %2.2f, f10 %2.2f, f11 %2.2f \n", f00, f01, f10, f11);
                printf("Pixel value warped image: (%d, %d): %4f\n", j, i, f);
            }
#endif
        }
    }

    return output;
}

SDoublePlane inverse3by3(const SDoublePlane &mat) {
    SDoublePlane result(3,3);
    //-------------------------
    // just to make life easier
    //-------------------------
    double &a=mat[0][0];
    double &b=mat[0][1];
    double &c=mat[0][2];
    double &d=mat[1][0];
    double &e=mat[1][1];
    double &f=mat[1][2];
    double &g=mat[2][0];
    double &h=mat[2][1];
    double &i=mat[2][2];
    //-----------------------------
    // calculate determinant of mat
    //-----------------------------
    //double detM = mat[0][0]*(mat[1][1]*mat[2][2]-mat[1][2]*mat[2][1])+
    //    mat[0][1]*(mat[1][2]*mat[2][0]-mat[1][0]*mat[2][2])+
    //    mat[0][2]*(mat[1][0]*mat[2][1]-mat[1][1]*mat[2][0]);
    double detM=a*(e*i-f*h)+b*(f*g-d*i)+c*(d*h-e*g);
    //printf("determinat of matrix: %f\n", detM);

    //-------------------
    // inverse hard coded
    //-------------------
    result[0][0] = (e*i-f*h)/detM;
    result[0][1] = (c*h-b*i)/detM;
    result[0][2] = (b*f-c*e)/detM;
    result[1][0] = (f*g-d*i)/detM;
    result[1][1] = (a*i-c*g)/detM;
    result[1][2] = (c*d-a*f)/detM;
    result[2][0] = (d*h-e*g)/detM;
    result[2][1] = (b*g-a*h)/detM;
    result[2][2] = (a*e-b*d)/detM;

    return result;
}
