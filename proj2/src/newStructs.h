//----------------------------------------------------------------------------
// This file contains structs defined for this program
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Feb 26th 2014 09:37:32 PM EST
//          Define coordinate, descriptor
// Last Modified: Sun,Mar 09th 2014 01:55:40 AM EST
//----------------------------------------------------------------------------

#ifndef _NEW_STRUCTS__H__
#define _NEW_STRUCTS__H__

#include"magicNum.h"
#include<cstdio>

// structure for a 2D point
class Coordinate {  
    public:
        int row, col;
        void print() {
            printf("(%d, %d)\n", col, row);
        }
        Coordinate& operator=(const Coordinate& rhs) {
            this->row = rhs.row;
            this->col = rhs.col;
            return *this;
        }
};

// structure for feature descriptor
class Descriptor {
    public:
        Coordinate pt;
        // should use int
        int vector[DESCRIPTOR_LENGTH];
        Descriptor() {
            clear();
        }
        inline void clear() {
            for(int i=0;i<DESCRIPTOR_LENGTH;i++) {
                vector[i]=0;
            }
            pt.row=0;
            pt.col=0;
        }
        void print() {
            printf("pixel location (%d, %d)\n", pt.col, pt.row);
            printf("------------\n");
            for(int i=0;i<DESCRIPTOR_LENGTH;i++) {
                printf("%d\t",vector[i]);
                if(i%8==7) {
                    printf("\n");
                }
            }
            printf("\n");
        }
};

// structure for matched point from two images
class MatchedPoint {
    public:
        // point in the target image
        Coordinate targetPt;
        // point in my image
        Coordinate myPt;

        // print
        void print() {
            printf(" my point: (%d, %d); target point (%d, %d) \n", myPt.col, myPt.row, targetPt.col, targetPt.row);
        }
};

#endif
