//----------------------------------------------------------------------------
// Contains function for computing feature vector given list of corner and
// original image
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Mar 06th 2014 01:53:17 PM EST
// Last Modified: Fri,Mar 07th 2014 12:39:09 AM EST
//----------------------------------------------------------------------------
#ifndef _FEATURE_VEC__H__
#define _FEATURE_VEC__H__
#include"newStructs.h"
#include"SImage.h"
#include<vector>

using namespace std;

// get the feature descriptors of an image
vector<Descriptor> getFeatureVec(const SDoublePlane &img, vector<Coordinate> &pts);

// get the 4 corner matrices
// pos can take 4 values, 0 (upleft), 1 (upright), 2 (downleft), 3 (downright)
// The correctness of this function can be checked by setting CORNER_MAT_SZ very
// large (e.g. 500) and output 4 separate images, and visually check or matlab
SDoublePlane getCornerMat(const SDoublePlane &img, Coordinate point, int pos);

//----------------------------------------------------------------------------
// Helper function
//----------------------------------------------------------------------------
// arc tangent, return 0 to 2*pi
double arcTan(double op1, double op2);
// square 
double square(double op1, double op2);

#endif
