ccc
A = load('../A');
A = sparse(A(:,1),A(:,2),A(:,3));
A = full(A);
A = A'
x = load('../x');
x = x';
b = load('../b');
b = b';

b_matlab = A*x;
x_matlab = A\b;

diffb=b_matlab-b;
diffx=x_matlab-x;
discrpb=max(max(diffb));
discrpb
discrpx=max(max(diffx));
discrpx
