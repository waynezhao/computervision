% ----------------------------------------------------------------------------
% very simple script to test the correctness of "getCornerMat" function
% Weiran (Ryan) Zhao 
% Started: Thu,Mar 06th 2014 11:38:54 PM EST
% Last Modified: Thu,Mar 06th 2014 11:57:57 PM EST
% ----------------------------------------------------------------------------
pictName = 'lincoln';
parentfolder=['../results/',pictName,'/'];
cmat0=imread([parentfolder, 'cornerMat0.png']);
cmat1=imread([parentfolder, 'cornerMat1.png']);
cmat2=imread([parentfolder, 'cornerMat2.png']);
cmat3=imread([parentfolder, 'cornerMat3.png']);
mat=[cmat0,cmat1;cmat2,cmat3];
imshow(mat)
