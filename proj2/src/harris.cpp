//----------------------------------------------------------------------------
// This file contains routines for doing harris corner detection
// (implementation)
//-----------
// Reference:
//-----------
//  [1] http://www.stat.wisc.edu/~mchung/teaching/MIA/reading/diffusion.gaussian.kernel.pdf
//  Gaussian kernel of 2D (or more) is separable as 2 (or more) 1D Gaussian
//  kernel
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Feb 26th 2014 09:31:16 PM EST
// Modified: Fri,Mar 07th 2014 12:54:11 AM EST
//           Simplify and fix calculation of imgXGrad and imgYGrad
//           X corresponds to column and Y corresponds to row
// Last Modified: Fri,Mar 07th 2014 12:55:00 AM EST
//----------------------------------------------------------------------------
#include"harris.h"
#include"filter.h"
#include"magicNum.h"
#include"convolve.h"
#include<cassert>

vector<Coordinate> harrisCorners(const SDoublePlane &input) {
    //----------------------
    // smooth image slightly
    //----------------------
    SDoublePlane gssnRow = filter::gaussian(1, GAUSSIAN_WND_SZ);
    SDoublePlane gssnCol = filter::gaussian(GAUSSIAN_WND_SZ, 1);
    SDoublePlane smoothedImg = convolve::convolve_separable(input, gssnRow, gssnCol);

    //--------------------
    // compute derivatives
    //--------------------
    SDoublePlane imgXGrad = convolve::convolve_general(smoothedImg, filter::diff_col());
    SDoublePlane imgYGrad = convolve::convolve_general(smoothedImg, filter::diff_row());

    //-----------------
    // compute matrix H
    //-----------------
    // compute Ix^2, Iy^2, IxIy
    SDoublePlane tmpIxsq = imgXGrad.eleWiseOpsWith(imgXGrad,SDoublePlane::mul);
    SDoublePlane tmpIysq = imgYGrad.eleWiseOpsWith(imgYGrad,SDoublePlane::mul);
    SDoublePlane tmpIxIy = imgXGrad.eleWiseOpsWith(imgYGrad,SDoublePlane::mul);
    // set up a window and do weighted sum, can use gaussian kernel 
    SDoublePlane Ixsq = convolve::convolve_separable(tmpIxsq, filter::gaussian(1, CORNER_WND_SZ), filter::gaussian(CORNER_WND_SZ,1));
    SDoublePlane Iysq = convolve::convolve_separable(tmpIysq, filter::gaussian(1, CORNER_WND_SZ), filter::gaussian(CORNER_WND_SZ,1));
    SDoublePlane IxIy = convolve::convolve_separable(tmpIxIy, filter::gaussian(1, CORNER_WND_SZ), filter::gaussian(CORNER_WND_SZ,1));
    
    //----------------------------------
    // compute corner response function 
    //----------------------------------
    // calculate determinant of H
    SDoublePlane tmp1 = Ixsq.eleWiseOpsWith(Iysq,SDoublePlane::mul);
    SDoublePlane tmp2 = IxIy.eleWiseOpsWith(IxIy,SDoublePlane::mul);
    SDoublePlane detH = tmp1.eleWiseOpsWith(tmp2,SDoublePlane::sub);
    // calculate trace of H
    SDoublePlane traceH = Ixsq.eleWiseOpsWith(Iysq,SDoublePlane::add);
    SDoublePlane traceHsq = traceH.eleWiseOpsWith(traceH,SDoublePlane::mul);
    SDoublePlane kTraceHsq = traceHsq.eleWiseScale(HARRIS_RESP_CONST);
    // harris corner response function R(x,y)=det(H)-k*trace(H)^2
    SDoublePlane harRespImg = detH.eleWiseOpsWith(kTraceHsq, SDoublePlane::sub);

    //----------------------------------------
    // non-maximal suppresing and thresholding
    //----------------------------------------
    SDoublePlane suppressedImg = harRespImg.nonMaxSuppress(CORNER_WND_SZ,CORNER_WND_SZ);
    SDoublePlane binImg = suppressedImg.binarize(HARRIS_RESP_THRESH);
    //SDoublePlane outImg = binImg.eleWiseScale(255);
    //SImageIO::write_png_file("outImg.png", outImg, outImg, outImg);

    //-----------------------------
    // locate all the corner pixels
    //-----------------------------
    vector<Coordinate> points = cornerPixels(binImg);

    //--------------------
    // return those points
    //--------------------
    return points;
}

vector<Coordinate> cornerPixels(const SDoublePlane &input) {
    vector<Coordinate> cornerPts;
    Coordinate tmp;
    
    //---------------------------
    // check each pixel to decide
    //---------------------------
    int thresh = CORNER_WND_SZ/2;
    int numRows = input.rows();
    int numCols = input.cols();
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            if((input[i][j]==1) && ((i+1)>thresh) &&((numRows-i)>thresh) && ((j+1)>thresh) && ((numCols-j)>thresh)) {
                tmp.row = i;
                tmp.col = j;
                cornerPts.push_back(tmp);
            }
        }
    }

    return cornerPts;
}
