//----------------------------------------------------------------------------
// Using ransac to find a homography between 2 images
//----------
// Reference
//----------
//      [1] http://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/EPSRC_SSAZ/node11.html
//          4 point method
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Sat,Mar 08th 2014 01:42:31 PM EST
// Last Modified: Sun,Mar 09th 2014 04:33:14 PM EDT
//----------------------------------------------------------------------------

#include"ransac.h"
#include"matrixOps.h"
#include<cstdlib>
#include<time.h>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<limits>

vector<MatchedPoint> ransacHomography(const vector<MatchedPoint> matchedPt) {
    //------------------------------
    // check number of matched point
    //------------------------------
    int listSz = matchedPt.size();
    if(listSz<4) {
        printf("Warning: we only have %d number of matched points, returning identity matrix\n", listSz);
        vector<MatchedPoint> result;
        MatchedPoint pt;
        for(int i=1;i<5;i++) {
            pt.targetPt.col = i;
            pt.targetPt.row = i;
            pt.myPt.col =i;
            pt.myPt.row = i;
            result.push_back(pt);
        }
        return result;
    }
    //------------------------------------------------------
    // loop till max iteration or break on early termination
    //------------------------------------------------------
    SDoublePlane bestHomo;
    SDoublePlane tmpHomo;
    int bestVt = 0;
    int tmpVt = 0;
    vector<MatchedPoint> best4pt;
    for(int i=0;i<RANSAC_MAX_ITER;i++) {
        //--------------------------------
        // randomly select 4 matched point
        //--------------------------------
        vector<MatchedPoint> fourPt;
        vector<MatchedPoint> tmpMPL = matchedPt;
        int rndIdx=0;
        int mplSz;
        for(int j=0;j<4;j++) {
            mplSz = tmpMPL.size();
            rndIdx = rand()%mplSz;
            fourPt.push_back(tmpMPL[rndIdx]);
            // remove this point from tmp list
            tmpMPL.erase(tmpMPL.begin()+rndIdx);
        }

        //----------------------------------------
        // compute homography using 4 point method
        //----------------------------------------
        tmpHomo = fourPtMethod(fourPt);

        //-------------------------
        // vote for this homography
        //-------------------------
        tmpVt = vote(tmpHomo, matchedPt);

        //----------------------------
        // see if we do an early break
        //----------------------------
        if(tmpVt > bestVt) {
            bestHomo = tmpHomo;
            bestVt = tmpVt;
            best4pt = fourPt;
        }
        if(bestVt > listSz*EARLY_TERM_RATIO) {
            printf("ransacHomography: early termintated at %d steps\n", i+1);
            break;
        }
    }

    printf("ransacHomography: best vote cnt: %d, total matched pt: %d, ratio: %.3f%%\n",bestVt, listSz, (double)(bestVt)/listSz*100);
    return best4pt;
}






//----------------------------------------------------------------------------
// Helper function implementation
//----------------------------------------------------------------------------
SDoublePlane fourPtMethod(vector<MatchedPoint> matchedPt) {
    //-----------------
    // check parameters
    //-----------------
    assert(matchedPt.size()==4);

    SDoublePlane result(3,3);
    //--------------------
    // declare A of Ax = b
    //--------------------
    int sizeA = 8;
    double *A = (double*) malloc(sizeof(double)*sizeA*sizeA);
    memset(A, 0, sizeof(double)*sizeA*sizeA);

    //---------------------
    // constructing A and b
    //---------------------
    Coordinate targetPt;
    Coordinate myPt;
    int tmp;
    for(int i=0;i<sizeA/2;i++) {
        targetPt = matchedPt[i].targetPt;
        myPt = matchedPt[i].myPt;
        //----------------------------------
        // constraints from x correspondence
        // x',y' for targetPt; x, y for myPt
        //----------------------------------
        tmp = sizeA*2*i;
        A[tmp] = myPt.col;
        A[tmp+1] = myPt.row;
        A[tmp+2] = 1;
        A[tmp+6] = -targetPt.col*myPt.col;
        A[tmp+7] = -targetPt.col*myPt.row;
        result.data_ptr()[2*i] = targetPt.col;
        //----------------------------------
        // constraints from y correspondence
        // x',y' for targetPt; x, y for myPt
        //----------------------------------
        tmp += sizeA;
        A[tmp+3] = myPt.col;
        A[tmp+4] = myPt.row;
        A[tmp+5] = 1;
        A[tmp+6] = -targetPt.row*myPt.col;
        A[tmp+7] = -targetPt.row*myPt.row;
        result.data_ptr()[2*i+1] = targetPt.row;
    }

    //--------------------------------
    // solve x = A\b (matlab notation)
    //--------------------------------
    //solveEq(sizeA, A, b);
    solveEq(sizeA, A, result.data_ptr());
    result.data_ptr()[8]=1;

    // free memory
    free(A); 

    return result;
}

vector<MatchedPoint> featureMatching(vector<Descriptor> targetList, vector<Descriptor> myList) {
    vector<MatchedPoint> result;
    //---------------------
    // useful tmp variables
    //---------------------
    int bestIdx, sndBestIdx, bestDist, sndBestDist, tmpDist;
    double distSq1, distSq2;
    /*printf("*********************************************\n");
    printf("printing target list\n");
    for(int i=0;i<targetList.size();i++) {
        targetList[i].print();
    }
    printf("*********************************************\n");
    printf("printing my list\n");
    for(int i=0;i<myList.size();i++) {
        myList[i].print();
    }*/

    MatchedPoint matched;
    for(int i=0;i<myList.size();i++) {
        bestDist = numeric_limits<int>::max();
        sndBestDist = numeric_limits<int>::max();
        bestIdx = -1;
        sndBestIdx= -1;
        // find the best descriptor in target list for myList[i]
        for(int j=0;j<targetList.size();j++) {
            tmpDist = distSq(myList[i].vector, targetList[j].vector, DESCRIPTOR_LENGTH);
            if( tmpDist < bestDist) {
                sndBestDist = bestDist;
                bestDist = tmpDist;
                sndBestIdx= bestIdx;
                bestIdx = j;
            } else if (tmpDist < sndBestDist) {
                sndBestDist = tmpDist;
                sndBestIdx = j;
            }
        }
        // decide if we want this descriptor
        distSq1 = (double)bestDist;
        distSq2 = (double)sndBestDist;
        if( distSq1/distSq2 < DISTSQ_NEAR_RATIO) {
            matched.myPt = myList[i].pt;
            matched.targetPt = targetList[bestIdx].pt;
            result.push_back(matched);
        } 
    }

    return result;
}

double distSq(const Coordinate &p1, const Coordinate &p2) {
    double sum=(double)((p1.col-p2.col)*(p1.col-p2.col)+(p1.row-p2.row)*(p1.row-p2.row));
    return sum;
}

int distSq(const int* vec1, const int* vec2, const int size) {
    int sum=0;
    for(int i=0;i<size;i++) {
        sum+=(vec1[i]-vec2[i])*(vec1[i]-vec2[i]);
    }
    return sum;
}

int vote(SDoublePlane homoMat, const vector<MatchedPoint> matchedList) {
    int cnt = 0;

    //--------------
    // b = homoMat*x
    //--------------
    double x[3], b[3];
    Coordinate estTarget;
    MatchedPoint tmp;
    //--------------------------------
    // loop through all matched points
    //--------------------------------
    for(int i=0;i<matchedList.size();i++) {
        tmp=matchedList[i];
        // construct homogeneous coordinates
        x[0] = (double) tmp.myPt.col;
        x[1] = (double) tmp.myPt.row;
        x[2] =1;
        // matrix vector multiply b=homoMat*x
        matVecMul(homoMat.data_ptr(), 3, 3, x, b);
        // use round, or int just truncate
        estTarget.col=(int)(round(b[0]/b[2]));
        estTarget.row=(int)(round(b[1]/b[2]));
        // decide if this one is a match
        if(distSq(estTarget, tmp.targetPt)<VOTE_NEAR_DISTSQ) {
            cnt++;
        }
    }
    return cnt;
}

