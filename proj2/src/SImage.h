//----------------------------------------------------------------------------
// originally from skeleton code "SImage.h"
// separate class declaration from implementation
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Jan 30th 2014 01:59:45 AM EST
// Modified: Wed,Mar 05th 2014 10:47:52 PM EST
//           Extend SDoublePlane adding element wise operation
//           eleWiseScale and eleWiseOpsWith
// Modified: Thu,Mar 06th 2014 12:17:34 AM EST
//           Add non maximal suppressing function
//           Mostly from project 1
// Last Modified: Fri,Mar 07th 2014 04:33:22 PM EST
//----------------------------------------------------------------------------
#ifndef __SIMAGE_H__
#define __SIMAGE_H__

#include"DTwoDimArray.h"

// A very simple image class.

class SDoublePlane : public _DTwoDimArray<double> {
    public:
        SDoublePlane() { }
        SDoublePlane(int _rows, int _cols);

        //-------------------------------------------------
        // element wise operation with "who"
        // operation can be multiply, add, and etc
        //-------------------------------------------------
        SDoublePlane eleWiseOpsWith(const SDoublePlane &who, double(*ops)(double,double));
        
        //-----------------------------
        // element wise scale operation
        //-----------------------------
        SDoublePlane eleWiseScale(const double ratio);

        //---------------------------------------
        // set a threshold and binarize 2D matrix
        //---------------------------------------
        SDoublePlane binarize(const double thres);

        //-------------------------------------------------
        // non maximal suppressing with rows-by-cols window
        // set non max element to value "nonMaxVal"
        //-------------------------------------------------
        SDoublePlane nonMaxSuppress(const int rows, const int cols, double nonMaxVal=0.0);

        // transpose an image
        SDoublePlane transpose();

        //----------------------------------------------------------------------------
        // Helper functions
        //----------------------------------------------------------------------------
        
        //----------------------------------------------
        // provide some simple operations for eleWiseOps
        // +, -, *, /
        //----------------------------------------------
        inline static double add(double op1, double op2) { return op1+op2;}
        inline static double sub(double op1, double op2) { return op1-op2;}
        inline static double mul(double op1, double op2) { return op1*op2;}
        inline static double div(double op1, double op2) { return op1/op2;}

        //---------------
        // print function
        //---------------
        void print();
};

#endif
