//----------------------------------------------------------------------------
// originally from skeleton code "SImage.h"
// separate class declaration from implementation
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Jan 30th 2014 01:59:45 AM EST
// Last Modified: Fri,Mar 07th 2014 04:35:12 PM EST
//----------------------------------------------------------------------------

#include"SImage.h"
#include<string.h>
#include<cassert>
#include<cstdio>
#include<algorithm>
#include<limits>

using namespace std;

SDoublePlane::SDoublePlane(int _rows, int _cols)  : _DTwoDimArray<double>(_rows, _cols) { 
    memset(data_ptr(), 0, sizeof(double) * rows() * cols());
}

SDoublePlane SDoublePlane::eleWiseOpsWith(const SDoublePlane &who, double(*ops)(double,double)) {
    //----------------------
    // do parameter checking
    //----------------------
    assert(this->rows()==who.rows());
    assert(this->cols()==who.cols());

    SDoublePlane result(this->rows(),this->cols());

    //------------------------
    // here is the heavy work
    //------------------------
    for(int i=0;i<this->rows();i++) {
        for(int j=0;j<this->cols();j++) {
            result[i][j]=ops(this->data[i][j],who[i][j]);
        }
    }

    return result;
}

SDoublePlane SDoublePlane::eleWiseScale(const double ratio) {
    SDoublePlane result(this->rows(),this->cols());

    //-------------------
    // element wise scale
    //-------------------
    for(int i=0;i<this->rows();i++) {
        for(int j=0;j<this->cols();j++) {
            result[i][j]=this->data[i][j]*ratio;
        }
    }

    return result;
}

SDoublePlane SDoublePlane::binarize(const double thres) {
    SDoublePlane result(this->rows(),this->cols());

    //--------------------------
    // element wise thresholding
    //--------------------------
    for(int i=0;i<this->rows();i++) {
        for(int j=0;j<this->cols();j++) {
            if(data[i][j]<thres) {
                result[i][j]=0;
            } else {
                result[i][j]=1;
            }
        }
    }

    return result;
}


SDoublePlane SDoublePlane::nonMaxSuppress(const int rows, const int cols, double nonMaxVal) {
    // rows and cols should always be odd number
    assert(rows%2==1);
    assert(cols%2==1);
    
    SDoublePlane output(this->rows(),this->cols());
    int row_radius = rows/2;
    int col_radius = cols/2;
    double nbhd_max;
    // for each pixel, check a rows-by-cols neighborhood
    for(int i=0;i<this->rows();i++) {
        for(int j=0;j<this->cols();j++) {
            nbhd_max = numeric_limits<double>::min();
            // find max within neighborhood
            for(int r=max(0,i-row_radius); r<=min(this->rows()-1,i+row_radius); r++) {
                for(int c=max(0,j-col_radius); c<=min(this->cols()-1,j+col_radius);c++) {
                    if(data[r][c] > nbhd_max) {
                        nbhd_max= data[r][c];
                    }
                }
            }
            // decide output
            if( data[i][j] ==nbhd_max) {
                output[i][j]=data[i][j];
            } else {
                output[i][j]=nonMaxVal;
            }
        }
    }
    return output;
}

SDoublePlane SDoublePlane::transpose() {
    SDoublePlane output(this->cols(),this->rows());
    for(int i=0;i<output.rows();i++) {
        for(int j=0;j<output.cols();j++) {
            output[i][j]=this->data[j][i];
        }
    }
    return output;
}


//----------------------------------------------------------------------------
// Helper function implementation
//----------------------------------------------------------------------------
void SDoublePlane::print() {
    for(int i=0;i<_rows;i++) {
        for(int j=0;j<_cols;j++) {
            printf("%.4e    ",data[i][j]);
        }
        printf("\n");
    }
}
