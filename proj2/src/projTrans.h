//----------------------------------------------------------------------------
// Perform a projective transformation given an image and transformation matrix
//-----------
// Reference:
//-----------
// [1] http://ardoris.wordpress.com/2008/07/18/general-formula-for-the-inverse-of-a-3x3-matrix/
//     following the method of 3-by-3 inverse matrix
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Fri,Mar 07th 2014 12:47:03 PM EST
// Last Modified: Fri,Mar 07th 2014 04:42:46 PM EST
//----------------------------------------------------------------------------
#ifndef _PROJ_TRANS__H__
#define _PROJ_TRANS__H__

#include"SImage.h"

// perform a projective transformation
// ghost is pixel value outside of original image's range
SDoublePlane projectiveTransformation(const SDoublePlane &img, SDoublePlane &pMat, double ghost=255.0);


//----------------------------------------------------------------------------
// Helper function
//----------------------------------------------------------------------------
SDoublePlane inverse3by3(const SDoublePlane &mat);
#endif
