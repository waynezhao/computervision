//----------------------------------------------------------------------------
// Some basic matrix operations by calling "Fortran Blas" interface
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Fri,Mar 07th 2014 02:08:55 PM EST
// Last Modified: Sun,Mar 09th 2014 12:04:07 PM EDT
//----------------------------------------------------------------------------
#include"matrixOps.h"
#include<cstdlib>
#include<cassert>
#include<cstring>
#include<cstdio>
void solveEq(int n, const double* A, double* b) {
    //--------------------------------------------------------
    // have to create a buffer for dgesv, or A will be changed
    //--------------------------------------------------------
    double *A_buf = (double*) malloc(sizeof(double)*n*n);

    // transpose the matrix
    for(int i=0;i<n;i++) {
        for(int j=0;j<n;j++) {
            A_buf[i*n+j]=A[j*n+i];
        }
    }

    //---------------
    // now call dgesv
    //---------------
    int one=1;
    int* piv=(int*)malloc(n*sizeof(int));
    int info;
    dgesv_(&n, &one, A_buf, &n, piv, b, &n, &info);

    //------------
    // free memory
    //------------
    free(piv);
    free(A_buf);
    //----------------------------------------------
    // make sure we solve this equation successfully
    //----------------------------------------------
    // assertion is too strong, just raise a warning
    //assert(info==0);
    if(info!=0) {
        //printf("Warning, input matrix A is rank deficient!\n");
    }
}

void matVecMul(const double* A, int rowA, int colA, double* x, double* b) {
    //------------------------------------
    // have to transpose A to make it work
    //------------------------------------
    double *A_buf = (double*)malloc(sizeof(double)*rowA*colA);
    for(int i=0;i<colA;i++) {
        for(int j=0;j<rowA;j++) {
            A_buf[i*rowA+j] = A[j*colA+i];
        }
    }
    //-----------
    // call dgemv
    //-----------
    char trans='N';
    double dOne=1.0;
    double dZero=0.0;
    int iOne = 1;
    dgemv_(&trans,          // do y=A'x+b [in]
           &rowA,           // # of rows of A [in]
           &colA,           // # of cols of A [in]
           &dOne,           // y=alpha*A'x+beta*b [in]
           A_buf,           // array containing A [in]
           &colA,           // leading dimension of A [in]
           x,               // vector x [in]
           &iOne,           // increment of x [in]
           &dZero,          // beta [in]
           b,               // output y [out]
           &iOne);          // increment of y
    
    // free memory
    free(A_buf);

    //------------
    // using loops
    //------------
    //double sum;
    //for(int i=0;i<rowA;i++) {
    //    sum=0.0;
    //    for(int j=0;j<colA;j++) {
    //        sum+=A[i*colA+j]*x[j];
    //    }
    //    b[i]=sum;
    //}

}


