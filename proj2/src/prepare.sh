#!/bin/bash

#----------------------------------------------------------------------------
# This simple script will 
#       1) convert non-png image to png and delete non-png images
#       2) generate a file called "inputFileNames", which can be later used by
#       "./pvt"; 
# User need to provide where the image sequence are
# Weiran (Ryan) Zhao 
# Started: Sat,Mar 08th 2014 11:02:58 PM EST
# Last Modified: Sun,Mar 09th 2014 12:54:05 PM EDT
#----------------------------------------------------------------------------

#----------------------
# check input argument
#----------------------
if [ $# -lt 1 ]; then
    echo "Wrong input argument"
    echo "$0 [image sequence folder loation]"
    exit 1
fi

#-------------------------
# image sequence location
#-------------------------
location=$1;
if [[ ${location: -1:1} != '/' ]]; then 
    location="$location/"
    echo "image sequece location: $location"
else
    echo "image sequece location: $location"
fi

#------------------------
# change to the location
#------------------------
if [ -d $location ]; then
    cd $1
    echo "changed to $PWD"
else
    echo "$location does not exist"
    exit 1
fi

#----------------------------------------------------------------------------
# make sure every file is png, if not convert them (only jpg or jpeg) to png
#----------------------------------------------------------------------------
# dealing with jpg
jpgCnt=$(find . -name "*.jpg" |wc -l)
if [ $jpgCnt -gt 0 ]; then
    echo "There are ${jpgCnt} jpg files to be converted"
    echo "Converting..."
    jpgFiles=$(find . -name "*.jpg" -print)
    for file in $jpgFiles; do
        name=`basename $file`
        echo $name
        convert ${name} ${name%jpg}png
    done
    echo "Done"
fi
# dealing with jpeg
jpegCnt=$(find . -name "*.jpeg" |wc -l)
if [ $jpegCnt -gt 0 ]; then
    echo "There are ${jpegCnt} jpeg files to be converted"
    echo "Converting..."
    jpegFiles=$(find . -name "*.jpeg" -print)
    for file in $jpegFiles; do
        name=`basename $file`
        echo $name
        convert ${name} ${name%jpeg}png
    done
    echo "Done"
fi

#-----------------------
# create inputFileNames
#-----------------------
fileName="../imageNames_${location%/}.txt"
echo $fileName
echo $location > $fileName
pngfiles=$(find . -name "*.png" -print)
for file in $pngfiles; do
    name=`basename $file`
    echo ${name%.png} >> $fileName
done

#-----------------------
# remove non-png images
#-----------------------
echo "Removing..."
rm -rvf *.jpg *.jpeg
echo "Done"

#--------------------------
# go back to parent folder
#--------------------------
cd ..
echo "Changing back to parent folder $PWD"
