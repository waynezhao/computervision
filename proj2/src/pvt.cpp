//----------------------------------------------------------------------------
// projective view transformation project
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 24th 2014 01:32:14 AM EST
// Last Modified: Sun,Mar 09th 2014 05:54:54 PM EDT
//----------------------------------------------------------------------------
#include<vector>
#include<iostream>
#include<fstream>
#include<cmath>
#include<map>
#include<cstdlib>
#include<algorithm>
#include<string>
#include<time.h>
// self defined
#include"SImage.h"
#include"SImageIO.h"
#include"newStructs.h"
#include"harris.h"
#include"featureVec.h"
#include"filter.h"
#include"projTrans.h"
#include"ransac.h"
#include"matrixOps.h"
// for creating dir
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

using namespace std;


void plotCornerImg(string inLoc, string orgFileName, string extra, string outLoc, const vector<Coordinate> &pts);
void plotWarpedImg(string inLoc, string orgFileName, string outLoc, SDoublePlane &pMat);
void testGetCornerMat(string outLoc, const SDoublePlane &img);
void test4ptMethod();
// this is for testing step 4 only, to generate the given projection matrix
SDoublePlane lincolnProjMat();

int main(int argc, char *argv[]) {
    //---------------
    // input checking
    //---------------
    if(argc != 2) {
        cerr<<"usage: "<<argv[0]<<" [image sequence names file] "<<endl;
        cerr<<"And the easist way to get the [image sequece names file] is to run ./prepare [image sequece folder location]"<<endl;
        return 1;
    }

    //-------------------------------------
    // open parameter file "inputFileNames"
    //-------------------------------------
    string paraFileName(argv[1]);
    ifstream fpara(paraFileName.c_str());
    vector<string> fileNameLst;
    string fnametmp, folderName;
    // get folder name
    getline(fpara,folderName);
    // get list of file names
    while(getline(fpara, fnametmp)) {
        fileNameLst.push_back(fnametmp);
        fnametmp.clear();
    }

    // read file
    fpara.close();

    //-------------------------------------------------
    // make sure we have at least 2 images to play with
    //-------------------------------------------------
    if( fileNameLst.size()<2) {
        cerr<<"We have less than 2 images in "<<folderName<<endl;
        return 1;
    }
    
    //-------------------------------------------------------------------
    // create result directory and corresponding subdirectory if necesary
    //-------------------------------------------------------------------
    string resDir="results";
    string subDir=resDir+'/'+paraFileName+'/';

    //-------------------
    // check if dir exist
    //-------------------
    struct stat st;
    if (stat(resDir.c_str(), &st) == -1) {
        cout<<"creating dir "<<resDir<<endl;
        mkdir(resDir.c_str(), 0755);
    }
    if (stat(subDir.c_str(), &st) == -1) {
        cout<<"creating subdir "<<subDir<<endl;
        mkdir(subDir.c_str(), 0755);
    }

    //---------------------------------------------------------------
    // read in first image, get corner points and feature vector list
    //---------------------------------------------------------------
    fnametmp.clear();
    fnametmp = folderName+fileNameLst[0]+".png";
    SDoublePlane targetImg = SImageIO::read_png_file(fnametmp.c_str());
    // get corner points
    vector<Coordinate> targetCornerPts=harrisCorners(targetImg);
    // save image with detect corners
    plotCornerImg(folderName, fileNameLst[0],"_corners", subDir,targetCornerPts);
    // test feature vector list
    vector<Descriptor> targetFeatureList = getFeatureVec(targetImg, targetCornerPts);
    cout<<"----------------------------------------------------------------------------"<<endl;
    cout<<"Main: Working on target (base) image: "<<fileNameLst[0]<<endl;
    cout<<"Main: Number of target corners detected: "<<targetFeatureList.size()<<endl;

    //--------------------------------
    // Dealing with the rest of images
    //--------------------------------
    //for(int i=1;i<2;i++) {
    for(int i=1;i<fileNameLst.size();i++) {
        cout<<"----------------------------------------------------------------------------"<<endl;
        cout<<"Main: Working on target (base) image: "<<fileNameLst[i]<<endl;
        fnametmp.clear();
        fnametmp = folderName+fileNameLst[i]+".png";
        SDoublePlane myImg = SImageIO::read_png_file(fnametmp.c_str());
        // get corner points
        vector<Coordinate> myCornerPts=harrisCorners(myImg);
        // save image with detect corners
        plotCornerImg(folderName, fileNameLst[i],"_corners", subDir,myCornerPts);
        // test feature vector list
        vector<Descriptor> myFeatureList= getFeatureVec(myImg, myCornerPts);
        cout<<"Main: Number of my corners detected: "<<myFeatureList.size()<<endl;
        cout<<"Main: Number of target corners detected: "<<targetFeatureList.size()<<endl;
        // matching
        vector<MatchedPoint> matchedList = featureMatching(targetFeatureList, myFeatureList);
        cout<<"Main: Number of matched corners: "<<matchedList.size()<<endl;
        // find the best homography
        vector<MatchedPoint> bestFpt= ransacHomography(matchedList);
        //vector<Coordinate> targetPts, myPts;
        //for(int j=0;j<bestFpt.size();j++) {
        //    targetPts.push_back(bestFpt[j].targetPt);
        //    myPts.push_back(bestFpt[j].myPt);
        //}
        //plotCornerImg(folderName, fileNameLst[i], "_corresp", subDir, myPts);
        //plotCornerImg(folderName, fileNameLst[0], fileNameLst[i], subDir, targetPts);
        SDoublePlane homo=fourPtMethod(bestFpt);
        // save projected Img
        plotWarpedImg(folderName, fileNameLst[i], subDir, homo);
    }

    return 0;
}


void plotCornerImg(string inLoc, string orgFileName, string extra, string outLoc, const vector<Coordinate> &pts) {
    // read in files
    SDoublePlane r,g,b;
    SImageIO::read_png_file((inLoc+orgFileName+".png").c_str(), r,g,b);

    // loop over the points and plot
    Coordinate tmp;
    for(int i=0;i<pts.size();i++) {
        tmp=pts[i];
        SImageIO::overlay_rectangle(r,tmp.row-1,tmp.col-1,tmp.row+1,tmp.col+1,255,1);
        SImageIO::overlay_rectangle(g,tmp.row-1,tmp.col-1,tmp.row+1,tmp.col+1,0,1);
        SImageIO::overlay_rectangle(b,tmp.row-1,tmp.col-1,tmp.row+1,tmp.col+1,0,1);
    }
    string resultName=outLoc+'/'+orgFileName+extra+".png";
    SImageIO::write_png_file(resultName.c_str(), r, g, b);
}

void testGetCornerMat(string outLoc, const SDoublePlane &img) {
    Coordinate point;
    point.row = 340;
    point.col = 512;
    string resultName;
    // image 0
    SDoublePlane cmat0 = getCornerMat(img,point, 0);
    resultName = outLoc+'/'+"cornerMat0.png";
    SImageIO::write_png_file(resultName.c_str(), cmat0, cmat0, cmat0);
    // image 1
    SDoublePlane cmat1 = getCornerMat(img,point, 1);
    resultName.clear();
    resultName = outLoc+'/'+"cornerMat1.png";
    SImageIO::write_png_file(resultName.c_str(), cmat1, cmat1, cmat1);
    // image 2
    SDoublePlane cmat2 = getCornerMat(img,point, 2);
    resultName.clear();
    resultName = outLoc+'/'+"cornerMat2.png";
    SImageIO::write_png_file(resultName.c_str(),cmat2, cmat2, cmat2);
    // image 3
    SDoublePlane cmat3 = getCornerMat(img,point, 3);
    resultName.clear();
    resultName = outLoc+'/'+"cornerMat3.png";
    SImageIO::write_png_file(resultName.c_str(), cmat3, cmat3, cmat3);
}

void plotWarpedImg(string inLoc, string orgFileName, string outLoc, SDoublePlane &pMat) {
    // read in files
    SDoublePlane r,g,b;
    SImageIO::read_png_file((inLoc+orgFileName+".png").c_str(), r,g,b);

    // project r, g, b plane
    SDoublePlane projR = projectiveTransformation(r, pMat, 0);
    SDoublePlane projG = projectiveTransformation(g, pMat, 0);
    SDoublePlane projB = projectiveTransformation(b, pMat, 0);

    string resultName;
    resultName=outLoc+'/'+orgFileName+"_projected.png";
    SImageIO::write_png_file(resultName.c_str(), projR, projG, projB);
}

//------------------------------------------------------------------------
// some interesting result: if rndBase=1,000,000, meaning huge picture! if
// matched feature points were generated randomly, it is very easy to get ill
// conditioned matrix when using 4 point method; both with respect to input A
// and output 3-by-3 matrix.
//------------------------------------------------------------------------
void test4ptMethod() {
    vector<MatchedPoint> matchPts;
    MatchedPoint tmp;
    srand(time(NULL));
    // test this for some times
    int testNum = 20000;
    int failCnt = 0;
    int printOutPt = testNum/10;
    int rndBase = 1000;
    for(int m=0;m<testNum;m++) {
        if(m%printOutPt==(printOutPt-1)){
            printf("reaching %d\n", m);
        }
        matchPts.clear();
        for(int i=0;i<4;i++) {
            tmp.myPt.col = rand()%rndBase;
            tmp.myPt.row = rand()%rndBase;
            tmp.targetPt.col = rand()%rndBase;
            tmp.targetPt.row = rand()%rndBase;
            matchPts.push_back(tmp);
        }

        // calculate transformation
        SDoublePlane mytrans=fourPtMethod(matchPts);

        // check if each point matches
        double x[3], b[3];
        Coordinate estTarget;
        for(int i=0;i<4;i++) {
            tmp=matchPts[i];
            x[0] = (double) tmp.myPt.col;
            x[1] = (double) tmp.myPt.row;
            x[2] =1;
            matVecMul(mytrans.data_ptr(), 3, 3, x, b);
            estTarget.col=(int)(round(b[0]/b[2]));
            estTarget.row=(int)(round(b[1]/b[2]));
            if(distSq(estTarget, tmp.targetPt)>1) {
                //mytrans.print();
                //printf("x is (%.2f, %.2f, %.2f)\n", x[0],x[1],x[2]);
                //printf("b is (%.2f, %.2f, %.2f)\n", b[0],b[1],b[2]);
                //printf("est_target: (%d, %d); real target: (%d, %d)\n", estTarget.col, estTarget.row, tmp.targetPt.col, tmp.targetPt.row);
                failCnt++;
                break;
            }
        }
    }
    printf("fail times %d, total trial %d\n", failCnt, testNum);
    printf("fail ratio is %.4f%%\n", (double)failCnt/testNum*100);
}

SDoublePlane lincolnProjMat() {
    SDoublePlane result(3,3);
    result[0][0]=0.907;
    result[0][1]=0.258;
    result[0][2]=-182;
    
    result[1][0]=-0.153;
    result[1][1]=1.440;
    result[1][2]=58;

    result[2][0]=-0.000306;
    result[2][1]=0.000731;
    result[2][2]=1;

    return result;
}
