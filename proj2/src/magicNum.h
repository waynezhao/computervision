//----------------------------------------------------------------------------
// Bunch of magic numbers for this program to run properly
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Wed,Feb 26th 2014 09:39:13 PM EST
// Last Modified: Sun,Mar 09th 2014 05:46:41 PM EDT
//----------------------------------------------------------------------------

#ifndef _MAGIC_NUM__H__
#define _MAGIC_NUM__H__

#define MY_PI 3.141592653589793238

//--------------------------------------------------------
// The following parameters are for Harris corner detector
//--------------------------------------------------------
// Gaussian smoother window size, need odd number
#define GAUSSIAN_WND_SZ 5

// Windown size for corner detection, when computing Ix, Iy
#define CORNER_WND_SZ 5

// constant k of Harris response function 
#define HARRIS_RESP_CONST 0.04

// threshold for harris reponse function
// Do remember this parameters control number of corner detected
// It will definitely affect the speed of RANSAC
// 1000000 is also a good threshold, but a lot of corners
#define HARRIS_RESP_THRESH 10000

//-----------------------------------------------------------
// The following parameters are for feature vector extraction
//-----------------------------------------------------------
// number of bins for each one of the 4 corner matrices
#define DESCRIP_BIN_SZ 16

// descriptor length (32 currently) should be 4 times DESCRIP_BIN_SZ
#define DESCRIPTOR_LENGTH 4*DESCRIP_BIN_SZ

// corner matrices size currently 8-by-8
// set this number to 500+ to test getCornerMat function
#define CORNER_MAT_SZ 12

// threshold for extracting descriptor into bins
// *********************************************
// need to be tuned
// *********************************************
#define DESCRIP_EXTRACT_THRESH 5

//---------------------------------------
// The following paramters are for RANSAC
//---------------------------------------
// this is for feature matching
#define DISTSQ_NEAR_RATIO 0.75

// vote allowed distance
#define VOTE_NEAR_DISTSQ 49

// max iteration of ransac
#define RANSAC_MAX_ITER 1000000

// early termination ratio for ransac
#define EARLY_TERM_RATIO 0.8
#endif
