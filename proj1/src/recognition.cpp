//----------------------------------------------------------------------------
// This files contains functions used to do recognition of stave lines, symbols
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 10th 2014 10:45:17 AM EST
// Modified: Tue,Feb 11th 2014 07:37:37 PM EST
//           use sobel filter from "filter" class in sobel_gradient_filter
// Last Modified: Thu,Feb 20th 2014 02:56:33 PM EST
//----------------------------------------------------------------------------
#include"filter.h"
#include"recognition.h"
#include"convolve.h"
#include"magicNum.h"
#include"SImageIO.h"
#include<iostream>
#include<cmath>
#include<climits>
#include<cstdlib>
#include<cstring>
#include<string>
#include<sstream>
#include<iomanip>

SDoublePlane sobel_gradient_filter(const SDoublePlane &input, bool xdir) {
    SDoublePlane output(input.rows(), input.cols());

    if(xdir) {
        output = convolve::convolve_separable(input, filter::sobel_sx_row(), filter::sobel_sx_col());
    } else {
        output = convolve::convolve_separable(input, filter::sobel_sy_row(), filter::sobel_sy_col());
    }
    return output;
}

SDoublePlane edgeStrength(const SDoublePlane &Sx, const SDoublePlane &Sy) {
    SDoublePlane output(Sx.rows(), Sx.cols());
    // error checking 
    // Sx and Sy should have same dimension
    if( (Sx.rows()!=Sy.rows()) || (Sx.cols()!=Sy.cols())) {
        cout<<"Warning: gradient images Sx and Sy not of same dimension"<<endl;
        return output;
    }
    for(int i=0;i<output.rows();i++) {
        for(int j=0;j<output.cols();j++) {
            output[i][j]=sqrt(Sx[i][j]*Sx[i][j]+Sy[i][j]*Sy[i][j]);
        }
    }
    return output;
}

SDoublePlane binarizeImg(const SDoublePlane &input, double thresh) {
    
    SDoublePlane output(input.rows(), input.cols());
    for(int i=0;i<input.rows();i++) {
        for(int j=0; j<input.cols();j++) {
            if(input[i][j]<=thresh) {
                output[i][j]=0;
            } else {
                //output[i][j]=255;
                output[i][j]=1;
            }
        }
    }

    return output;
}

SDoublePlane revBinarizeImg(const SDoublePlane &input, double thresh) {
    
    SDoublePlane output(input.rows(), input.cols());
    for(int i=0;i<input.rows();i++) {
        for(int j=0; j<input.cols();j++) {
            if(input[i][j]<=thresh) {
                output[i][j]=1;
            } else {
                //output[i][j]=255;
                output[i][j]=0;
            }
        }
    }

    return output;
}

SDoublePlane sobelEdge(const SDoublePlane &input, double thresh) {
    SDoublePlane Sx= sobel_gradient_filter(input,true);
    SDoublePlane Sy= sobel_gradient_filter(input,false);
    SDoublePlane edgestr = edgeStrength(Sx, Sy);
    SDoublePlane output=binarizeImg(edgestr, thresh);
    // fix boundary, make them non edge
    for(int i=0;i<output.rows();i++) {
        output[i][0]=0;
        output[i][output.cols()-1]=0;
    }
    for(int j=0;j<output.cols();j++) {
        output[0][j]=0;
        output[output.rows()-1][j]=0;
    }
    return output;
}

vector<int> getPossLines(const SDoublePlane &input, double thresh) {
    vector<int> result;
    double sum;
    // rescale threshold
    thresh=thresh*input.cols();
    for(int i=0;i<input.rows();i++) {
        sum=0.0;
        for(int j=0;j<input.cols();j++) {
            sum+=input[i][j];
        }
        // Normalize: I used 255 when do binarizeImg for pixel value
        //sum/=255;
        if(sum>thresh) {
            result.push_back(i);
        }
    }
    return result;
}

vector<int> getSqueezedLines(const vector<int> &input, double thresh) {
    vector<int> result;
    int sz=input.size();
    if(sz<1) {
        cout<<"Warning: size of input vector less than 1"<<endl;
        return result;
    }
    result.push_back(input[0]);
    int tmp;
    for(int i=1;i<sz;i++) {
        // smaller than thresh, then squeeze by taking mean
        if(abs(input[i]-result.back()) <=thresh) {
            tmp=result.back();
            result.pop_back();
            result.push_back((tmp+input[i])/2);
        } else {
            result.push_back(input[i]);
        }
    }
    return result;
}


double getSpacing(const vector<int> &input) {
    //-------------------------------
    // get difference of input vector
    //-------------------------------
    vector<int> diff=diffVec(input);

    //--------------------------------------
    // get maximun of diff and allocate bins
    //--------------------------------------
    int maxdiff=arrayMax(diff);
    int *bins = (int*)malloc(maxdiff*sizeof(int));
    memset(bins, 0, maxdiff*sizeof(int));

    //--------------
    // count of bins
    //--------------
    for(int i=0;i<diff.size();i++) {
        bins[diff[i]]++;
    }

    //------------------------
    // get max occurance value
    //------------------------
    int maxOccur = arrayMax(bins,maxdiff);

    //-----------------------------------
    //search index of max occurance value
    //-----------------------------------
    vector<int> maxIdx;
    for(int i=0;i<maxdiff;i++) {
        if(bins[i]==maxOccur) {
            maxIdx.push_back(i);
        }
    }

    //-------------------------
    // check this maxIdx vector
    //-------------------------
    if(maxIdx.size()>2) {
        cout<<"Warning: there are more than three possible spacing!"<<endl;
        printVec(maxIdx);
        vector<int> tmp;
        // prune out those impossibles
        for(int i=0;i<maxIdx.size();i++) {
            if( (maxIdx[i]>5) && (maxIdx[i]<=20)) {
                tmp.push_back(maxIdx[i]);
            }
        }
        // return the mean
        if(tmp.size()>=1) {
            return arrayMean(tmp);
        } else {
            cout<<"Warning: we are in deep trouble finding spacing, ";
            cout<<"select 2nd one of many possibles"<<endl;
            return maxIdx[1];
        }
    }
    if(maxIdx.size()==2) {
        // check if values are far apart
        if(abs(maxIdx[0]-maxIdx[1])>2) {
            cout<<"Warning: possible spacings far apart!"<<endl;
            return double(maxIdx[0]);
        } else { // return mean value
            return (double(maxIdx[0])+double(maxIdx[1]))/2;
        }
    }
    return double(maxIdx[0]);
}

vector<vector<int> > groupLines(const vector<int> &input, double spacing, double thresh) {
    // rescale threshold
    thresh = thresh*spacing;
    vector<vector<int> > result;
    vector<int> tmp;
    tmp.push_back(input[0]);
    for(int i=1;i<input.size();i++) {
        // if distance too big, we found a group
        if(abs(input[i]-tmp.back())>thresh) {
            if(tmp.size()>MIN_STAVE_SZ) {
                result.push_back(tmp);
            }
            tmp.clear();
            tmp.push_back(input[i]);
        } else { // just push back to tmp
            tmp.push_back(input[i]);
        }
    }
    if(tmp.size()>MIN_STAVE_SZ) {
        result.push_back(tmp);
    }
    return result;
}

vector<vector<int> > refineLines(const vector<vector<int> > &input, double spacing, double thresh) {
    vector<vector<int> > result;
    // rescale threshold
    thresh = spacing*thresh;
    vector<int> tmp1;
    vector<int> tmp2;
    for(int i=0;i<input.size();i++) {
        tmp1 = input[i];
        // in this method, we put a lot of faith in the first line
        tmp2.clear();
        tmp2.push_back(tmp1[0]);
        for(int j=1;j<tmp1.size();j++) {
            if( abs(tmp1[j]-tmp2.back())<thresh) {
                // do nothing
            } else {
                tmp2.push_back(tmp1[j]);
            }
        }
        result.push_back(tmp2);
    }
    return result;
}


SDoublePlane getTemplate(double spacing, int which) {
    // do error checking
    if( (which <1) || (which >3) ) {
        cout<<"WARNING: template index out of range, returning null template"<<endl;
        SDoublePlane zero(0,0);
        return zero;
    }

    // which folder
    int folderId=nearestOdd(spacing);

    //----------------------------------------
    // just hard code file path name into code
    //----------------------------------------
    ostringstream oss;
    oss<<"templates/"<<setfill('0')<<setw(2);
    oss<<folderId<<"/template";
    oss.width(1);
    oss<<which;
    oss<<".png";

    SDoublePlane tmplt = SImageIO::read_png_file(oss.str().c_str());
    //SImageIO::write_png_file("tmplt.png", tmplt, tmplt, tmplt);
    return tmplt;
}

SDoublePlane scaleBinImg(const SDoublePlane &input) {
    SDoublePlane output(input.rows(),input.cols());
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            if(input[i][j]==1) {
                output[i][j]=255;
            } else {
                output[i][j]=0;
            }
        }
    }
    return output;
}

SDoublePlane symNonMaxSuppress(const SDoublePlane &input, int win_rows, int win_cols) {
    // win_rows and win_cols should always be column number
    if( (win_rows%2==0) || (win_cols%2==0)) {
        cout<<"Warning: win_rows or win_cols not odd number, enforcing that"<<endl;
        win_rows = nearestOdd(win_rows);
        win_cols = nearestOdd(win_cols);
    }
    SDoublePlane output(input.rows(),input.cols());
    int row_radius = win_rows/2;
    int col_radius = win_cols/2;
    double nbhd_max;
    // for each pixel, check a win_rows-by-win_cols neighborhood
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            nbhd_max = VERY_NEGATIVE;
            // find max within neighborhood
            for(int r=max(0,i-row_radius); r<=min(input.rows()-1,i+row_radius); r++) {
                for(int c=max(0,j-col_radius); c<=min(input.cols()-1,j+col_radius);c++) {
                    if(input[r][c] > nbhd_max) {
                        nbhd_max= input[r][c];
                    }
                }
            }
            // decide output
            if( input[i][j] ==nbhd_max) {
                output[i][j]=input[i][j];
            } else {
                output[i][j]=0;
            }
        }
    }
    return output;
}

vector<detectedSymbol> getSymbolList(const SDoublePlane &input, int height, int width, int thresh, int typeidx) {
    vector<detectedSymbol> list;
    // loop through each pixel of input and see if its value is 1
    detectedSymbol sym;
    sym.height = (int)(height*1.1);
    sym.width = (int)(width*1.1);
    sym.type= (Type) (typeidx);
    // just initialize pitch and confidence
    sym.pitch = 'A';
    sym.confidence = 1;
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            if(input[i][j]==1) {
                //------------------------------------------------------------
                // I don't know why, but there are height/2 and width/2 offset
                // for each of the center of symbol I detected. Have to fix it
                // here
                // Found it!
                // It turns out it is the problem with David's code of
                // write_detected_image
                // I will bear this off-set
                //------------------------------------------------------------
                sym.row=max(0,i-height/2);
                sym.col=max(0,j-width/2);
                insertSymbol(list,thresh,sym);
            }
        }
    }
    return list;
}

void assignPitch(const vector<vector<int> > &staves, double spacing, vector<detectedSymbol>& syms) {
    vector<linePitch> pitchs = getLinePitchList(staves,spacing);
    // for each symbo in syms scan the 'pitchs' list to find the closet line
    int sym_size = syms.size();
    int sym_idx = 0;
    int dist_min;
    int min_idx;
    int tmp;
    while(sym_idx<sym_size) {
        dist_min = 10000;
        min_idx = -1;
        for(int i=0;i<pitchs.size();i++) {
            if( (tmp=abs(syms[sym_idx].row-pitchs[i].row)) < dist_min) {
                dist_min = tmp;
                min_idx = i;
            }
        }
        // check if this distance is way off
        if( dist_min > int(spacing)) {
            // should delete this deteced symbol
            syms.erase(syms.begin()+sym_idx);
            sym_size--;
        } else {
            syms[sym_idx].pitch = pitchs[min_idx].pitch;
            //syms[sym_idx].confidence = pitchs[min_idx].confidence;
            sym_idx++;
        }
    }
}

SDoublePlane nearestPixel(const SDoublePlane& input) {
    SDoublePlane output(input.rows(),input.cols());
    // get a list of all pixel that is edge
    vector<point> edgeList;
    point pt;
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            if( input[i][j]==1) {
                pt.row=i;
                pt.col=j;
                edgeList.push_back(pt);
            }
        }
    }
    cout<<"number of edge pixels "<<edgeList.size()<<endl;
    double min_ele, tmp1,tmp2,tmp3;
    for(int i=0;i<input.rows();i++) {
        for(int j=0;j<input.cols();j++) {
            if( input[i][j]==1) {
                output[i][j]=0;
            } else {
                min_ele = VERY_POSITIVE*VERY_POSITIVE;
                for(int k=0;k<edgeList.size();k++) {
                    tmp1 = edgeList[k].row-i;
                    tmp2 = edgeList[k].col-j;
                    if( (tmp3=tmp1*tmp1+tmp2*tmp2) < min_ele) {
                        min_ele=tmp3;
                    }
                } 
                output[i][j]=sqrt(min_ele);
            }
        }
    }
    return output;
}



//----------------------------------------------------------------------------
// Helper functions implementation
//----------------------------------------------------------------------------
int arrayMax(const vector<int> &input) {
    int max=INT_MIN;
    for(int i=0;i<input.size();i++) {
        if(input[i]>max) {
            max=input[i];
        }
    }
    return max;
}

int arrayMax(const int* input, const int len) {
    int max=INT_MIN;
    for(int i=0;i<len;i++) {
        if(input[i]>max) {
            max=input[i];
        }
    }
    return max;
}

void printVec(const vector<int> &input) {
    cout<<"--------------------------------------------"<<endl;
    for(int i=0;i<input.size();i++) {
        if(i%10==9) {
            cout<<"--------------------------------------------"<<endl;
        }
        cout<<"\t"<<input[i];
    }
    cout<<endl;
}

vector<int> diffVec(const vector<int> &input) {
    vector<int> result;
    if(input.size()<=1) {
        cout<<"Warning: can not do differencing for vector of length 1 or smaller"<<endl;
        return result;
    }
    for(int i=1;i<input.size();i++) {
        result.push_back(input[i]-input[i-1]);
    }
    return result;
}

double arrayMean(const vector<int> &input) {
    double sum=0.0;
    int sz=input.size();
    for(int i=0;i<sz;i++) {
        sum+=input[i];
    }
    return sum/sz;
}

int nearestOdd(double num) {
    return floor(floor(num)/2)*2+1;
}

double notXor(double op1, double op2) {
    return op1*op2+(1-op1)*(1-op2);
}

void insertSymbol(vector<detectedSymbol> & symList, int thresh, const detectedSymbol sym) {
    if(symList.size()==0) {
        symList.push_back(sym);
        return;
    } else {
        for(int i=0;i<symList.size();i++) {
            // check if there's any symbol A in the list that is very near to
            // "sym", both x and y difference should fall into thresh range
            if( (abs(sym.row-symList[i].row) <=thresh) &&
                (abs(sym.col-symList[i].col) <=thresh)) {
                // we can use the mean value or we can just return
                symList[i].row = (symList[i].row+sym.row)/2;
                symList[i].col = (symList[i].col+sym.col)/2;
                return;
            }
        }
        // no body in the list near "sym", insert "sym"
        symList.push_back(sym);
        return;
    }
}

vector<linePitch> getLinePitchList(vector<vector<int> > staves, double spacing) {
    vector<linePitch> result;
    int firstLine;
    // 0 for low
    int highOrLow=0;
    linePitch lp;
    int baseLinePitch[2]={0,5};

    for(int i=0;i<staves.size();i++) {
        if(staves[i].size()!=5) {
            lp.confidence = 0.5;
        } else {
            lp.confidence = 1.0;
        }
        firstLine = staves[i][0];
        highOrLow = (highOrLow+1)%2;

        for(int i=-2;i<5+2; i++) {
            lp.row = int(firstLine+i*spacing);
            lp.pitch = 'A'+(baseLinePitch[highOrLow]+(i*-2)-1+21)%7;
            result.push_back(lp);
            lp.row = int(firstLine+i*spacing+spacing/2);
            lp.pitch = 'A'+(baseLinePitch[highOrLow]+(i*-2)-1-1+21)%7;
            result.push_back(lp);
        }
    }

    return result;
}

double gammaFunc(double num) {
    if(num==0) {
        return VERY_POSITIVE;
    } else {
        return 0;
    }
}
