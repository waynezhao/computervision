//----------------------------------------------------------------------------
// Implementation for some convolution methods
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 03th 2014 03:36:30 PM EST
//          Separate functions from omr.cpp
// Modified: Mon,Feb 03th 2014 03:43:31 PM EST
//           Extend convolve_general to handle non-square kernel, but still with
//           odd dimension
// Modified: Wed,Feb 05th 2014 12:42:35 AM EST
//           Implement separable kernel convolution
// Last Modified: Thu,Feb 13th 2014 06:43:12 PM EST
//----------------------------------------------------------------------------
#include"convolve.h"
#include<iostream>

using namespace std;

SDoublePlane convolve::convolve_general(const SDoublePlane &input, const SDoublePlane &filter, double (*ops)(double,double)) {
    //--------------------
    // Create output image
    //--------------------
    SDoublePlane output(input.rows(), input.cols());
    //----------------------------
    // do some error checking here
    //----------------------------
    //--------------------------------------------------
    // filter is square and number of rows should be odd
    //--------------------------------------------------
    if( (filter.cols()%2==0) || (filter.rows()%2 ==0)) {
        // just return input image and printer warning
        cout<<"Warning: filter not odd dimension, can't handle"<<endl;
        return output;
    }

    //--------------
    // ghost element
    //--------------
    double ghost=0.0;
    double sum;
    double ele;
    // radius of kernel
    int col_radius = filter.cols()/2;
    int row_radius = filter.rows()/2;
    //-------------------------
    // loop through every pixel
    //-------------------------
    for(int i=0;i<input.rows();i++) {
        for(int j=0; j<input.cols(); j++) {
            //------------------------------------
            // loop through each element of kernel
            //------------------------------------
            sum=0.0;
            for(int m=-row_radius; m<=row_radius; m++) {
                for(int n=-col_radius; n<=col_radius; n++) {
                    //-------------------------
                    // check boundary condition
                    //-------------------------
                    if( (i-m) <0 || (j-n) <0 || (i-m) >=input.rows() || (j-n) >= input.cols()) {
                        ele = ghost;
                    } else {
                        ele = input[i-m][j-n];
                    }
                    //sum+=filter[m+row_radius][n+col_radius]*ele;
                    sum+=ops(filter[m+row_radius][n+col_radius],ele);
                }
            }
            output[i][j]=sum;
        }
    }
    return output;
}


SDoublePlane convolve::convolve_separable(const SDoublePlane &input, const SDoublePlane &row_filter, const SDoublePlane &col_filter) {
    //--------------------
    // create output image
    //--------------------
    SDoublePlane output(input.rows(), input.cols());

    //------------------------
    // let's do error checking
    //------------------------
    // row filter should have num-of-row =1
    if( row_filter.rows()!=1) {
        cout<<"Warning: row filter is NOT a row vector"<<endl;
        // return immediately
        return output;
    }
    // col filter should have num-of-col =1
    if( col_filter.cols()!=1) {
        cout<<"Warning: col filter is NOT a col vector"<<endl;
        return output;
    }

    //----------------------------------------------------------
    // since convolution is communative, so it should not matter
    // row/col filter applies first
    //----------------------------------------------------------
    // create a tmp image first
    SDoublePlane tmpImg(input.rows(), input.cols());

    // apply row filter first
    tmpImg = convolve_general(input, row_filter);
    // then column
    output = convolve_general(tmpImg, col_filter);

    return output;
}
