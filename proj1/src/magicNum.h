//----------------------------------------------------------------------------
// This files contains all magic numbers (threshold numbers) for this paricular
// project. God knows why it is what it is
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 10th 2014 01:03:02 AM EST
// Last Modified: Fri,Feb 14th 2014 08:38:44 PM EST
//----------------------------------------------------------------------------

#ifndef _MAGIC_NUM_H_
#define _MAGIC_NUM_H_

//============================================================
// I found several set of working parameters
// 1) SOBEL_EDGE_THRESH 8.0
//    STAVE_LINE_RATIO 0.25
// 2) SOBEL_EDGE_THRESH 7.5
//    STAVE_LINE_RATIO 0.3
//============================================================
// threshold for find edge using sobel operator
#define SOBEL_EDGE_THRESH 7.5

// threshold for deciding if one row of pixels is a line
#define STAVE_LINE_RATIO 0.25

// threshold for squeeze neighborhood lines
#define SQUEEZE_LINE_THRESH 3

// at least 4, 5, etc lines group will be considered as a stave
#define MIN_STAVE_SZ 3

// maximal distance allowance threshold
#define MAX_DIST_ALLOW 2

// minimal distance allowance threshold
#define MIN_DIST_ALLOW 0.75

// this is for when inserting symbol to symbol list
#define TOO_NEAR_RATIO 0.3

// this is for finding symbol
#define SYM_THRESH 0.80

// this is for binarizing template and input image
// 150 good for template3
// 125 good for template2
#define BIN_THRESH_TPL1 200
#define BIN_THRESH_TPL2 140
#define BIN_THRESH_TPL3 160

// a big negative and positive number
#define VERY_NEGATIVE -10000000.0
#define VERY_POSITIVE 10000000.0
#endif
