%----------------------------------------------------------------------------
% This matlab script is used for checking if my convolution methods is working
% correctly. 
% The idea is simple:
%       1) for original image A, convolve it with a kernel K using my code to
%       generate output A_c;
%       2) Use the built-in matlab function *conv2* to work on A and K and get
%       the image A_m; (use 'same' option to get output image same size as input
%       3) Do max(max(A_m-A_c)) to report the maximun discrepancy
% The following kernels have been tested:
%       1) 3-by-3 mean kernel
%       2) 5-by-5 mean kernel
%       3) 5-by-5 filter of shape 1/75*[1,2,3,4,5; 1,2,3,4,5;...;1,2,3,4,5]
%       4) 5-by-5 gaussian filter (matlab built-in fspecial())
%       5) 5-by-3 mean kernel
%       6) 3-by-5 mean kernel
%============================================================
% Weiran (Ryan) Zhao 
% Started: Mon,Feb 03th 2014 02:52:57 PM EST
% Modified: Wed,Feb 05th 2014 12:59:43 AM EST
%           Fix bug about integer +/- can not use uint8 to do +/-, minus value
%           will be truncated to 0
% Modified: Wed,Feb 05th 2014 01:10:18 AM EST
%           Test separable kernel convolve, following has been tested:
%           1) 1-by-5 row, 5-by-1 col mean kernel
% Last Modified: Mon,Feb 10th 2014 12:59:31 AM EST
%----------------------------------------------------------------------------

%------------------
% clear everything
%------------------
ccc

img_name = '../scores/music1.png';
c_img_name = '../c_convolve.png';

%---------------
% read in image
%---------------
A = imread(img_name);
A_c = imread(c_img_name);

%-----------------
% form the kernel
%-----------------
%K = 1/9*ones(3,3);
%K = 1/25*ones(5,5);
%K = 1/75*repmat(1:5,5,1);
%K = fspecial('gaussian',5);
%K = 1/15*ones(5,3);
%K = 1/15*ones(3,5);

%------------------------------------------
% transform image to the correct data type
%------------------------------------------
A = rgb2gray(A);
A_c = rgb2gray(A_c);

%-------------------------------
% convolve using matlab routine
%-------------------------------
%A_m = conv2(double(A),K,'same');

%----------------------------
% this is separable convolve
%----------------------------
row = 1/5*ones(1,5);
col = 1/5*ones(5,1);
A_m = conv2(row,row,double(A),'same');
A_m = uint8(A_m);

%--------------------
% report discrepancy
%--------------------
max(max(abs(int16(A_m)-int16(A_c))))
max(max(abs(int16(A_c)-int16(A_m))))
figure
imshow(A_m);
figure
imshow(A_c);
