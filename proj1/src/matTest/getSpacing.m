%----------------------------------------------------------------------------
% Get spacing of stave
% ============================================================
% Weiran (Ryan) Zhao 
% Started: Sun,Feb 09th 2014 11:02:51 PM EST
% Last Modified: Sun,Feb 09th 2014 11:11:10 PM EST
%----------------------------------------------------------------------------
function spacing = getSpacing(list)
    maxele = max(list);
    bins = zeros(maxele,1);
    for i=1:length(list) 
        bins(list(i))=bins(list(i))+1;
    end
    top = max(bins);
    topidx = find(bins==top);
    spacing = mean(topidx);
end
