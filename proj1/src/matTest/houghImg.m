%----------------------------------------------------------------------------
% This script is used to test hough transform idea, before write a C++
% equivelant of it
%============================================================
% Weiran (Ryan) Zhao 
% Started: Sat,Feb 08th 2014 03:56:13 PM EST
%          Developed several wrong versions of hough transform for a set of 5
%          lines
% Modified: Sat,Feb 08th 2014 10:36:17 PM EST
%           Finish sobel operator and find the best threshold
% Modified: Sun,Feb 09th 2014 08:07:29 PM EST
%           Try method from David of hough transform, result still not very
%           prominent, decide to use other heuristic
% Last Modified: Mon,Feb 10th 2014 12:59:56 AM EST
%----------------------------------------------------------------------------

%------------------
% clear everything
%------------------
ccc

img_name = '../scores/music1.png';
%img_name = '../scores/music2.png';
%img_name = '../scores/music3.png';
%img_name = '../scores/music4.png';
%img_name = '../scores/rach.png';


%---------------
% read in image
%---------------
A = imread(img_name);

%------------------------------------------
% transform image to the correct data type
%------------------------------------------
if(ndims(A)==3) 
    A = rgb2gray(A);
end

%--------------
% get the edge
%--------------

% This is to call matlab's edge detector
%imgEdge= edge(A,'sobel',0.15);

%-------------------------------------------------
% This is to write sobel filter and set threshold
%-------------------------------------------------
sx1=[-1,0,1]/8;
sx2=[1,2,1]/8;
sy1=[1,2,1]/8;
sy2=[1,0,-1]/8;
A=double(A);
Sx= conv2(sx1,sx2,A);
Sy= conv2(sy1,sy2,A);
imgEdge = sqrt(Sx.*Sx+Sy.*Sy);
fprintf('maximun value of imgEdge is %f\n',max(max(imgEdge)));
thresh = 7.5;
imgEdge(imgEdge<=thresh)=0;
imgEdge(imgEdge>thresh)=1;
% image edge done

edgeA = uint16(imgEdge);
n_rows = size(edgeA,1);

%------------------------------
% version 4 of hough transform
%------------------------------
%n_rows = size(edgeA,1);
%n_cols = size(edgeA,2);
%min_gap = 5;
%max_gap = ceil(n_rows/10);
%cumul = zeros(n_rows,max_gap);
%for m = 1:n_rows
%    for n = 1:n_cols
%        if(edgeA(m,n)==1) 
%            ridx = m;
%            % 5 possible position
%            for j=1:5
%                for gap=min_gap:max_gap
%                    start = ridx-(j-1)*gap;
%                    if start < 1
%                        break;
%                    else 
%                        cumul(start,gap)=cumul(start,gap)+1;
%                    end
%                end
%            end
%        end
%    end
%end
%mval = max(max(cumul))
%[si,sj]=find(cumul==mval)
%figure, imshow(imadjust(mat2gray(cumul))), hold on, colormap(hot), hold off;
%figure, imshow(imgEdge);
%hold on
%hline=line([1,n_cols],[31,31]);
%set(hline,'Color','r');
%hline=line([1,n_cols],[164,164]);
%set(hline,'Color','r');
%hold off

%-------------------------------------
% version 3 of hough transform failed
%-------------------------------------
%----------------------------------
% identify index of possible lines
%----------------------------------
edgeSum = sum(edgeA,2);
lineThreshold = 0.25*size(A,2);
possLine = find(edgeSum>lineThreshold);
%---------------
% squeeze lines
%---------------
squeezeThres = 3;
squeezLine = zeros(length(possLine));
squeezLine(1,1) = possLine(1,1);
counter = 1;
for i=2:length(possLine)
    if(abs(possLine(i)-squeezLine(counter))<=squeezeThres) 
        squeezLine(counter)=int32((squeezLine(counter)+possLine(i))/2);
    else
        counter = counter +1;
        squeezLine(counter)=possLine(i);
    end
end
squeezLine=squeezLine(squeezLine>0);

n_rows = size(edgeA,1);
min_gap = 5;
max_gap = ceil(n_rows/10);
cumul = zeros(n_rows,max_gap);
edgeVal = edgeSum(squeezLine);

for i = 1:length(squeezLine)
    ridx = possLine(i);
    % 5 possible position
    for j=1:5
        for gap=min_gap:max_gap
            start = ridx-(j-1)*gap;
            if start < 1
                break;
            else 
                cumul(start,gap)=cumul(start,gap)+edgeVal(i);
            end
        end
    end
end
mval = max(max(cumul))
[si,sj]=find(cumul==mval)
cumul(si,sj)=0;
mval = max(max(cumul))
[si,sj]=find(cumul==mval)
figure, imshow(imadjust(mat2gray(cumul))), hold on, colormap(hot), hold off;
figure, imshow(imgEdge);

%--------------------------------------
% version 2 (wrong) of hough transform
%--------------------------------------
%len = length(possLine);
%spsz = floor(n_rows/2);
%space = zeros(spsz,spsz);
%
%for l=1:len
%    for i = 0:4
%        y=possLine(l);
%        for d=1:spsz
%            b = int32(y-i*d);
%            %b = floor(b/2);
%            if(b>=1 && b<=spsz)
%                space(b,d)=space(b,d)+1;
%            end
%        end
%    end
%end
%mspace=max(max(space))
%[si,sj]=find(space==mspace)
%[si,sj]=find(space==mspace-1)
%figure,imshow(space)


%---------------------------------------------
% This part of code is for plot line on image
%---------------------------------------------
%squeezeThres = 3;
%squeezLine = zeros(length(possLine));
%squeezLine(1,1) = possLine(1,1);
%counter = 1;
%for i=2:length(possLine)
%    if(abs(possLine(i)-squeezLine(counter))<=squeezeThres) 
%        squeezLine(counter)=int32((squeezLine(counter)+possLine(i))/2);
%    else
%        counter = counter +1;
%        squeezLine(counter)=possLine(i);
%    end
%end
%figure
%squeezLine=squeezLine(squeezLine>0);
%lineend = size(edgeA,2);
%imshow(imgEdge);
%hold on
%for i=1:length(squeezLine)
%    hline=line([1,lineend],[squeezLine(i),squeezLine(i)]);
%    set(hline,'Color','r');
%end
%hold off
%
%--------------------------------------
% version 1 (wrong) of hough transform
%--------------------------------------
%cnt_A = zeros(n_rows,n_rows);
%for i = 1:n_rows
%    if(edgeSum(i,1)~=0)
%        for row = 1:i-1
%            for sp = 2:i-1
%                if(mod(i-row,sp)==0) 
%                    cnt_A(sp,row)=cnt_A(sp,row)+edgeSum(i,1);
%                end
%            end
%        end
%    end
%end
%
%contour(cnt_A);
