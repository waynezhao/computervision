%----------------------------------------------------------------------------
% figuring out all the images size and make sure dimensions are odd
% Then batch processing to generate the corresponding image size using 
%       imresize
% Dump the output to ../templates/[num]/ folder directly
%---------------
% WARNING!!!!!!
%---------------
% I didn't do any error checking, if run other place, make sure you understand
% what the code is doing
%============================================================
% Weiran (Ryan) Zhao 
% Started: Thu,Feb 13th 2014 07:18:15 PM EST
% Last Modified: Thu,Feb 13th 2014 09:39:11 PM EST
%----------------------------------------------------------------------------

ccc

%------------------------
% read in original image
%------------------------
namePrefix = '../templates/11/template';
for tmpltIdx=1:3
    fprintf('============================================================\n');
    fprintf('for template %d\n',tmpltIdx);
    tmpltName = sprintf('%s%d.png',namePrefix, tmpltIdx);
    %-------------------
    % read in the image
    %-------------------
    orgImg = imread(tmpltName);
    orgSz = size(orgImg);
    orgSz = orgSz(1:2);

    %-------------------------------------------------------
    % just want to generate for 5, 7, 9, 13, 15, 17, 19, 21
    %-------------------------------------------------------
    for i=5:2:21
        % 11 is our original one
        if(i==11)
            continue;
        end
        ratio = i/11;
        %--------------------------------
        % nearest odd number of a double
        %--------------------------------
        %ratio*orgSz
        newTmplt=floor(floor(ratio*orgSz)/2)*2+1;

        %-----------------
        %resize the image
        %-----------------
        newImg = imresize(orgImg,newTmplt);

        %----------
        % save it!
        %----------
        newImgName=sprintf('../templates/%02d/template%d.png',i,tmpltIdx);
        imwrite(newImg, newImgName);

        %-----------
        % some info
        %-----------
        fprintf('Saved image %s\n', newImgName);
    end
end
