%----------------------------------------------------------------------------
% This script is to find groups of lines of a music score
%============================================================
% Weiran (Ryan) Zhao 
% Started: Sun,Feb 09th 2014 08:14:24 PM EST
% Last Modified: Fri,Feb 14th 2014 01:48:37 AM EST
%----------------------------------------------------------------------------

%------------------
% clear everything
%------------------
ccc

img_name = '../scores/music1.png';
img_name = '../scores/music2.png';
img_name = '../scores/music3.png';
img_name = '../scores/music4.png';
%img_name = '../scores/rach.png';

% scores I found
%img_name = '../otherScores/bach1.png';
%img_name = '../otherScores/bach2.png';
%img_name = '../otherScores/bach3.png';

%---------------
% read in image
%---------------
A = imread(img_name);

%------------------------------------------
% transform image to the correct data type
%------------------------------------------
if(ndims(A)==3) 
    A = rgb2gray(A);
end

%--------------
% get the edge
%--------------

% This is to call matlab's edge detector
%imgEdge= edge(A,'sobel',0.10);

%-------------------------------------------------
% This is to write sobel filter and set threshold
%-------------------------------------------------
%A=conv2(A,1/9*ones(3,3),'same');
A=double(A);
sx1=[-1,0,1]/8;
sx2=[1,2,1]/8;
sy1=[1,2,1]/8;
sy2=[1,0,-1]/8;
Sx= conv2(sx1,sx2,A);
Sx= Sx(2:end-1,2:end-1);
Sy= conv2(sy1,sy2,A);
Sy= Sy(2:end-1,2:end-1);
imgEdge = sqrt(Sx.*Sx+Sy.*Sy);
fprintf('maximun value of imgEdge is %f\n',max(max(imgEdge)));
thresh = 7.5;
imgEdge(imgEdge<=thresh)=0;
imgEdge(imgEdge>thresh)=1;
% image edge done

%-------------------------------------
% short test of result of C vs matlab
%-------------------------------------
%sob = imread('../sobel.png');
%sob = rgb2gray(sob);
%imgEdge=imgEdge.*255;
%d=double(sob)-double(imgEdge);
%md=max(max(abs(d)))
%[si,sj]=find(d==md)

edgeA = uint16(imgEdge);
n_rows = size(edgeA,1);


%---------------------------------------------
% This part of code is for plot line on image
%---------------------------------------------
edgeSum = sum(edgeA,2);
lineThreshold = 0.25*size(A,2);
possLine = find(edgeSum>lineThreshold);
squeezeThres = 3;
squeezLine = zeros(length(possLine));
squeezLine(1,1) = possLine(1,1);
%------------------------
% squeeze neary by lines
%------------------------
counter = 1;
for i=2:length(possLine)
    if(abs(possLine(i)-squeezLine(counter))<=squeezeThres) 
        squeezLine(counter)=floor((squeezLine(counter)+possLine(i))/2);
    else
        counter = counter +1;
        squeezLine(counter)=possLine(i);
    end
end
squeezLine=squeezLine(squeezLine>0);

%---------------------
% find groups of line
%---------------------
diffSqueeze = squeezLine(2:end)-squeezLine(1:end-1);
spacing=getSpacing(diffSqueeze)

% cut points based on too wide
cutPts = find(diffSqueeze>2*spacing);
cutPts = [0; cutPts; length(squeezLine)];

MIN_STAVE_SZ = 2;
newLine={};
counter = 1;
for i=1:length(cutPts)-1
    tmp=squeezLine(cutPts(i)+1:cutPts(i+1));
    disp('-------------');
    tmp
    if length(tmp) > MIN_STAVE_SZ
        newLine{counter}=tmp;
        counter=counter+1;
    end
end

% cut points based on too narrow
finalLines=[];
startLines=[];
newLine{:,:}
for i=1:length(newLine)
    tmp = newLine{:,i};
    startLines=[startLines;tmp(1)];
    idx=2;
    while(idx<=length(tmp))
        d = tmp(idx)-tmp(idx-1);
        if (d < 0.75*spacing) 
            tmp(idx)=[];
        else 
            idx= idx+1;
        end
    end
    finalLines=[finalLines;tmp];
    disp('-----------');
    tmp
end

lineend = size(edgeA,2);
figure,imshow(imgEdge);
hold on
for i=1:length(finalLines)
    hline=line([1,lineend],[finalLines(i),finalLines(i)]);
    set(hline,'Color','r');
end
hold off


