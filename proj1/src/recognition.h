//----------------------------------------------------------------------------
// This files contains functions used to do recognition of stave lines, symbols
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 10th 2014 10:45:17 AM EST
// Modified: Mon,Feb 10th 2014 11:20:43 AM EST
//           Add functions to get possible Lines, squeezeLine
// Last Modified: Fri,Feb 14th 2014 06:45:57 PM EST
//----------------------------------------------------------------------------
#ifndef _RECOGNITION_H_
#define _RECOGNITION_H_
#include"SImage.h"
#include"detectedSymbol.h"
#include<vector>

using namespace std;

//--------------------------------------------------------------------
// do a sobel filter, if xdir=true, do x direction; otherwise for ydir
// for x direction
//--------------------------------------------------------------------
SDoublePlane sobel_gradient_filter(const SDoublePlane &input, bool xdir);

//---------------------------------------------------------
// given gradient images Sx and Sy, calculate edge strength
//---------------------------------------------------------
SDoublePlane edgeStrength(const SDoublePlane &Sx, const SDoublePlane &Sy);

//----------------------------------
// Using thresh to binarize an image
// and reverse binarize image
//----------------------------------
SDoublePlane binarizeImg(const SDoublePlane &input, double thresh=0);
SDoublePlane revBinarizeImg(const SDoublePlane &input, double thresh=0);

//---------------------------------
// use sobel operator to find edges
//---------------------------------
SDoublePlane sobelEdge(const SDoublePlane &input, double thresh);

//-------------------------------------------------------------
// get possible lines index
// if the edge pixel on a line is greater than thresh*num_cols,
// then consider it as a line, record its index
//-------------------------------------------------------------
vector<int> getPossLines(const SDoublePlane &input, double thresh);

//---------------------------------------------------------------
// squeeze lines if possible, because each line of a stave 
// has width, so both top and bottom of each line were recognized
//---------------------------------------------------------------
vector<int> getSqueezedLines(const vector<int> &input, double thresh);

//--------------------------------------------------------------
// get spacing of stave lines, very naive method to do statistic
// on input
//--------------------------------------------------------------
double getSpacing(const vector<int> &input);

//----------------------------------------------------------
// group lines by maximal distance allowance of nearby lines
// spacing*thresh is maximal distance allowance
//----------------------------------------------------------
vector<vector<int> > groupLines(const vector<int> &input, double spacing, double thresh);

//-----------------------------------------------------------------
// refine lines, if two lines are too close with respect to spacing
// Just delete them
//-----------------------------------------------------------------
vector<vector<int> > refineLines(const vector<vector<int> > &input, double spacing, double thresh);

//--------------------------------------------------------------------------
// given a spacing, and template id, readin the corresponding template image
//--------------------------------------------------------------------------
SDoublePlane getTemplate(double spacing, int which);

//-------------------------------------------------------------------------
// because binarized image are 0, 1 need to scale 1 to 255 to show normally
//-------------------------------------------------------------------------
SDoublePlane scaleBinImg(const SDoublePlane & input);

//---------------------------------------------
// non maximal suppressing for detected symbols
//---------------------------------------------
SDoublePlane symNonMaxSuppress(const SDoublePlane &input, int win_rows, int win_cols);

//---------------------------------------
// get a list of detected symbol position
//---------------------------------------
vector<detectedSymbol> getSymbolList(const SDoublePlane &input, int h, int w, int thresh, int type);

//--------------------------------------------------------------------------
// given stave lines, spacing and detected symbols, assign picth for symbols
//--------------------------------------------------------------------------
void assignPitch(const vector<vector<int> >& staves, double spacing, vector<detectedSymbol>& syms);

//----------------------------------
// computer nearest pixel for step 5
//----------------------------------
SDoublePlane nearestPixel(const SDoublePlane& input);






//-----------------------------------------------------------------------------
// helper functions
//----------------------------------------------------------------------------
// helper function to get max element of an vector array
int arrayMax(const vector<int> &input);
int arrayMax(const int* input, int len);
double arrayMean(const vector<int> &input);

void printVec(const vector<int> &input);

vector<int> diffVec(const vector<int> &input);

// given a double, find the nearest odd number to it
int nearestOdd(double num);

// not xor
double notXor(double op1, double op2);

// insert a symbol to symbol list, use thresh to control 
// whether or not to insert
void insertSymbol(vector<detectedSymbol> & symList, int thresh, const detectedSymbol sym);

class linePitch {
    public:
        int row;
        char pitch;
        double confidence;
};

// generate this linePitch lists
// given group of staves
vector<linePitch> getLinePitchList(vector<vector<int> > staves, double spacing);

// gamma function for computing score
double gammaFunc(double num);
// to represent a point
class point {
    public: 
        int row;
        int col;
};
#endif
