//----------------------------------------------------------------------------
// Extract detected symbol class from omr.cpp
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 03th 2014 04:00:14 PM EST
//          Extract detected symbol class from omr.cpp
// Last Modified: Mon,Feb 03th 2014 04:07:27 PM EST
//----------------------------------------------------------------------------

#ifndef _DETECTED_SYMBOL_H_
#define _DETECTED_SYMBOL_H_

typedef enum {NOTEHEAD=0, QUARTERREST=1, EIGHTHREST=2} Type;
class detectedSymbol {
    public:
        int row, col, width, height;
        Type type;
        char pitch;
        double confidence;
};
#endif
