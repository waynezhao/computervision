//----------------------------------------------------------------------------
// This class defines some commonly used filters such as
//          1) 2-D mean filter of any size
//          2) 1-D row/column mean filter since mean filter can be decomposed
//          3) gaussian filter
//          4) sobel filter for Sx and Sy
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Tue,Feb 11th 2014 05:09:25 PM EST
// Last Modified: Fri,Feb 14th 2014 08:01:08 PM EST
//----------------------------------------------------------------------------
#include"filter.h"
#include<iostream>
#include<cmath>
#include<cstdlib>
#include<cstdio>

using namespace std;

SDoublePlane filter::mean2d(int num_row, int num_col) {
    //--------------------------------------------------
    // error checking: only generate filter with odd dim
    //--------------------------------------------------
    if( (num_row%2==0) || (num_col%2==0)) {
        cout<<"Warning: only generate filter with odd dimension"<<endl;
        SDoublePlane wrong(0,0);
        return wrong;
    }
    SDoublePlane result(num_row, num_col);
    double ratio=1.0/(num_row*num_col);
    for(int i=0;i<num_row;i++) {
        for(int j=0;j<num_col;j++) {
            result[i][j]=ratio;
        }
    }
    return result;
}


SDoublePlane filter::mean1d_row(int length) {
    //--------------------------------------------------
    // error checking: only generate filter with odd dim
    //--------------------------------------------------
    if( length%2==0) {
        cout<<"Warning: only generate filter with odd dimension"<<endl;
        SDoublePlane wrong(0,0);
        return wrong;
    }
    SDoublePlane result(1, length);
    double ratio=1.0/length;
    for(int i=0;i<length;i++) {
            result[0][i]=ratio;
    }
    return result;
}

SDoublePlane filter::mean1d_col(int length) {
    //--------------------------------------------------
    // error checking: only generate filter with odd dim
    //--------------------------------------------------
    if( length%2==0) {
        cout<<"Warning: only generate filter with odd dimension"<<endl;
        SDoublePlane wrong(0,0);
        return wrong;
    }
    SDoublePlane result(length, 1);
    double ratio=1.0/length;
    for(int i=0;i<length;i++) {
            result[i][0]=ratio;
    }
    return result;
}


SDoublePlane filter::gaussian(int num_row, int num_col, double sigma) {
    //--------------------------------------------------
    // error checking: only generate filter with odd dim
    //--------------------------------------------------
    if( (num_row%2==0) || (num_col%2==0)) {
        cout<<"Warning: only generate filter with odd dimension"<<endl;
        SDoublePlane wrong(0,0);
        return wrong;
    }

    SDoublePlane result(num_row, num_col);
    // radius of kernel
    int row_radius = floor(num_row/2);
    int col_radius = floor(num_col/2);
    // this is for normalizing gaussian kernel
    double sum=0.0;

    // calculate "raw" value of kernel
    for(int i=-row_radius; i<=row_radius;i++) {
        for(int j=-col_radius; j<=col_radius;j++) {
            result[i+row_radius][j+col_radius]=g(double(i),double(j),sigma);
            sum+=result[i+row_radius][j+row_radius];
        }
    }

    //-----------------
    // normalize kernel
    //-----------------
    for(int i=0;i<num_row;i++) {
        for(int j=0;j<num_col;j++) {
            result[i][j]/=sum;
        }
    }
    return result;
}

// for x direction
// [ 1 ]                  [ -1 0 1 ]
// [ 2 ]  * [-1, 0, 1] =  [ -2 0 2 ]
// [ 1 ]                  [ -1 0 1 ]
SDoublePlane filter::sobel_sx() {
    SDoublePlane Sx(3,3);
    double ratio=1/8.0;
    Sx[0][0] = -1*ratio;
    Sx[0][1] = 0.0;
    Sx[0][2] = 1*ratio;
    Sx[1][0] = -2*ratio;
    Sx[1][1] = 0.0;
    Sx[1][2] = 2*ratio;
    Sx[2][0] = -1*ratio;
    Sx[2][1] = 0.0;
    Sx[2][2] = 1*ratio;
    return Sx;
}

SDoublePlane filter::sobel_sx_row() {
    SDoublePlane sx_row(1,3);
    double ratio = 0.125;
    sx_row[0][0] = -1*ratio;
    sx_row[0][1] = 0.0;
    sx_row[0][2] = 1*ratio;
    return sx_row;
}

SDoublePlane filter::sobel_sx_col() {
    SDoublePlane sx_col(3,1);
    double ratio = 0.125;
    sx_col[0][0] = ratio;
    sx_col[1][0] = 2*ratio;
    sx_col[2][0] = ratio;
    return sx_col;
}

// for y direction
// [ 1 ]                  [  1  2  1 ]
// [ 0 ]  * [1, 2, 1] =   [  0  0  0 ]
// [-1 ]                  [ -1 -2 -1 ]
SDoublePlane filter::sobel_sy(){
    SDoublePlane Sy(3,3);
    double ratio=1/8.0;
    Sy[0][0] = 1*ratio;
    Sy[0][1] = 2*ratio;
    Sy[0][2] = 1*ratio;
    Sy[1][0] = 0.0;
    Sy[1][1] = 0.0;
    Sy[1][2] = 0.0;
    Sy[2][0] = -1*ratio;
    Sy[2][1] = -2*ratio;
    Sy[2][2] = -1*ratio;
    return Sy;
}

SDoublePlane filter::sobel_sy_row() {
    SDoublePlane sy_row(1,3);
    double ratio = 0.125;
    sy_row[0][0] = ratio;
    sy_row[0][1] = 2*ratio;
    sy_row[0][2] = ratio;
    return sy_row;
}

SDoublePlane filter::sobel_sy_col() {
    SDoublePlane sy_col(3,1);
    double ratio = 0.125;
    sy_col[0][0] = ratio;
    sy_col[1][0] = 0.0;
    sy_col[2][0] = -1*ratio;
    return sy_col;
}

//============================================================
// Helper functions implementations
//============================================================
void filter::printFilter(const SDoublePlane &filter) {
    for(int i=0;i<filter.rows();i++) {
        for(int j=0;j<filter.cols();j++) {
            printf("%.4e    ",filter[i][j]);
        }
        printf("\n");
    }
}


double filter::g(double x, double y, double sigma) {
    double sigmaSq=sigma*sigma;
    return 1/(2*MY_PI*sigmaSq)*exp(-(x*x+y*y)/(2*sigmaSq));
}


SDoublePlane filter::flip(const SDoublePlane& input) {
    int row=input.rows();
    int col=input.cols();
    SDoublePlane output(row,col);
    for(int i=0;i<row;i++) {
        for(int j=0;j<col;j++) {
            output[i][j]=input[row-1-i][col-1-j];
        }
    }
    return output;
}

