//----------------------------------------------------------------------------
// originally from skeleton code "SImage.h"
// separate class declaration from implementation
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Jan 30th 2014 01:59:45 AM EST
// Last Modified: Thu,Jan 30th 2014 02:06:20 AM EST
//----------------------------------------------------------------------------
#ifndef __SIMAGE_H__
#define __SIMAGE_H__

#include"DTwoDimArray.h"

// A very simple image class.

class SDoublePlane : public _DTwoDimArray<double>
{
    public:
        SDoublePlane() { }
        SDoublePlane(int _rows, int _cols);
};

#endif
