//----------------------------------------------------------------------------
// originally from skeleton code "SImage.h"
// separate class declaration from implementation
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Thu,Jan 30th 2014 01:59:45 AM EST
// Last Modified: Thu,Jan 30th 2014 02:03:57 AM EST
//----------------------------------------------------------------------------

#include"SImage.h"
#include<string.h>

SDoublePlane::SDoublePlane(int _rows, int _cols)  : _DTwoDimArray<double>(_rows, _cols)
{ 
    memset(data_ptr(), 0, sizeof(double) * rows() * cols());
}
