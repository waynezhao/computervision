//----------------------------------------------------------------------------
// Class for some convolution methods
//------------
// Reference: 
//------------
//      [1]. http://stackoverflow.com/questions/5715220/what-is-usually-done-with-the-boundary-pixels-of-the-image
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Mon,Feb 03th 2014 03:36:30 PM EST
//          Separate functions from omr.cpp
// Modified: Mon,Feb 03th 2014 03:42:55 PM EST
//          According to [1], there are generally 4 methods to deal with ghost
//          element: 
//              1) padding 0s, 
//              2) duplicate first/last column or row
//              3) padding reflex columns or rows row(-1)=row(1), row(-2)=row(2) 
//              4) wrap padding row(-1)=row(total-1), row(-2)=row(total-2)
//          I checked matlab, it seems like matlab is doing 0 paddings, as of
//          now, I only implemented the 1). 
// Modified: Thu,Feb 13th 2014 06:42:26 PM EST
//           extend convolve_general to take an extra parameter called "ops",
//           which is a function pointer that can specify what operation you
//           want the kernel matrix and the image to have, e.g |, &, ^(xor) etc
//           The default is set to multiplication
// Last Modified: Thu,Feb 13th 2014 06:48:47 PM EST
//----------------------------------------------------------------------------
#ifndef _CONVOLVE_H_
#define _CONVOLVE_H_

#include"SImageIO.h"
class convolve {
    public:
        //--------------------------------------------------------------
        // convolve an image with a general kernel using brute force way
        // for ghost elements, just padding in 0s
        //--------------------------------------------------------------
        static SDoublePlane convolve_general(const SDoublePlane &input, const SDoublePlane &filter, double (*ops)(double,double)=mul);
        //----------------------------------------------------
        // convolve an image with a dimension separable kernel
        //----------------------------------------------------
        static SDoublePlane convolve_separable(const SDoublePlane &input, const SDoublePlane &row_filter, const SDoublePlane &col_filter);
    private:
        static double mul(double op1, double op2) {
            return op1*op2;
        }
};
#endif
