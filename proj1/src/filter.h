//----------------------------------------------------------------------------
// This class defines some commonly used filters such as
//          1) 2-D mean filter of any size
//          2) 1-D row/column mean filter since mean filter can be decomposed
//          3) gaussian filter
//          4) sobel filter for Sx and Sy
// 
//----------
// Reference
//----------
//      [1] for gaussian filter function
//      http://www.librow.com/articles/article-9
//      [2] for the idea of normalizing gaussian
//      http://www.programming-techniques.com/2013/02/gaussian-filter-generation-using-cc.html
//      [3] for gaussian sigma defaults to 0.5 conforms to matlab
//========================================
// Weiran Zhao, Computer Science Dept
// Indiana University, Bloomington
//========================================
// Started: Tue,Feb 11th 2014 05:09:25 PM EST
// Last Modified: Fri,Feb 14th 2014 07:57:01 PM EST
//----------------------------------------------------------------------------
#ifndef ___FILTER_H___
#define ___FILTER_H___

#include"SImage.h"

#define MY_PI 3.141592653589793238

class filter {
    public:
        // create a num_row-by-num_col 2D matrix
        static SDoublePlane mean2d(int num_row, int num_col);

        // create a 1d row filter
        static SDoublePlane mean1d_row(int length);

        // create a 1d col filter
        static SDoublePlane mean1d_col(int length);

        // create a gaussian filter
        static SDoublePlane gaussian(int num_row, int num_col, double sigma=0.5);

        // create sobel Sx
        static SDoublePlane sobel_sx();
        static SDoublePlane sobel_sx_row();
        static SDoublePlane sobel_sx_col();

        // create sobel Sy
        static SDoublePlane sobel_sy();
        static SDoublePlane sobel_sy_row();
        static SDoublePlane sobel_sy_col();

        // flip a kernel updown and left right
        static SDoublePlane flip(const SDoublePlane& input);

        //============================================================
        // Helper functions
        //============================================================
        static void printFilter(const SDoublePlane &filter);
    private:
        // this is the gaussian function
        static double g(double x, double y, double sigma);
};
#endif
