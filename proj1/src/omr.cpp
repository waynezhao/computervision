//----------------------------------------------------------------------------
// Originally from skeleton code
//-----------
// Reference:
//-----------
//  [1] http://stackoverflow.com/questions/5523042/how-to-get-the-stem-of-a-filename-from-a-path
//      http://www.cplusplus.com
//      On how to use string class to find characters
//  [2] http://stackoverflow.com/questions/7430248/how-can-i-create-new-folder-using-c-language
//      http://stackoverflow.com/questions/12510874/how-can-i-check-if-a-directory-exists-on-linux-in-c
//      On how to create dir if necessary
//============================================================
// Weiran Zhao
// Started: Mon,Feb 03th 2014 10:58:18 AM EST
// Modified: Wed,Feb 05th 2014 01:43:08 AM EST
//           Test separable kernel convolution
// Modified: Sun,Feb 09th 2014 03:10:16 PM EST
//           Added sobel filter, and edge thresholding function
// Last Modified: Fri,Feb 14th 2014 09:13:24 PM EST
//----------------------------------------------------------------------------
#include"filter.h"
#include"magicNum.h"
#include"recognition.h"
#include"convolve.h"
#include"SImage.h"
#include"SImageIO.h"
#include"DrawText.h"
#include<cmath>
#include<algorithm>
#include<iostream>
#include<fstream>
#include<vector>
#include<sstream>
#include<dirent.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

using namespace std;

// this is where all the result png get stored
string subDir; 

void testGeneralConvolve(SDoublePlane& input_image);
void testSeparateConvolve(SDoublePlane& input_image);
void testSobel(SDoublePlane& input_image);
// this function take original images and mark detected staves with blue lines
// this function returns groups of stave positions and a spacing
vector<vector<int> > getStaves(const SDoublePlane& input_image, double* spacing);
// using step 4 to find symbols
void findSymbolNaive(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves);
// using step 5 to find symbols
void findSymbolSmart(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves);
// put all pieces together
void finale(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves);
int main(int argc, char *argv[]) {
    //----------------------
    // check input arguments
    //----------------------
    if(!(argc == 2)){
        cerr << "usage: " << argv[0] << " input_image" << endl;
        return 1;
    }

    //--------------------
    // read in input files
    //--------------------
    SDoublePlane inputImg= SImageIO::read_png_file(argv[1]);
    //-------------------------------------------------------------------
    // create result directory and corresponding subdirectory if necesary
    //-------------------------------------------------------------------
    string fileloc(argv[1]);
    size_t pos1 = fileloc.find_last_of('/');
    size_t pos2 = fileloc.find_last_of('.');
    string fName;
    fName.assign(fileloc.begin()+pos1+1,fileloc.begin()+pos2);
    string resDir="results";
    subDir=resDir+'/'+fName;

    //-------------------
    // check if dir exist
    //-------------------
    struct stat st;
    if (stat(resDir.c_str(), &st) == -1) {
        cout<<"creating dir "<<resDir<<endl;
        mkdir(resDir.c_str(), 0755);
    }
    if (stat(subDir.c_str(), &st) == -1) {
        cout<<"creating subdir "<<subDir<<endl;
        mkdir(subDir.c_str(), 0755);
    }

    //-----------------------
    // filter the input image
    //-----------------------
    //SDoublePlane inputImg = convolve::convolve_general(tmp, filter::gaussian(3,3));

    //----------------------------------
    // this is where all the work begins
    //----------------------------------
    double spacing;
    cout<<"Working on step (6): stave lines spacing"<<endl;
    vector<vector<int> > rfLines=getStaves(inputImg,&spacing);
    cout<<"Working on step (4): Hamming distance"<<endl;
    findSymbolNaive(inputImg,spacing, rfLines);
    cout<<"Working on step (5): using edge maps"<<endl;
    findSymbolSmart(inputImg,spacing,rfLines);
    cout<<"Working on step (7): putting all piece together"<<endl;
    finale(inputImg,spacing, rfLines);
}

void testGeneralConvolve(SDoublePlane& input_image) {
    int n_row=3;
    int n_col=5;
    SDoublePlane myFilter=filter::gaussian(n_row,n_col);
    //SDoublePlane myFilter=filter::mean2d(n_row,n_col);
    SDoublePlane output_image = convolve::convolve_general(input_image, myFilter);
    SImageIO::write_png_file("c_convolve.png", output_image, output_image, output_image);
}

void testSeparateConvolve(SDoublePlane& input_image) {
    int row_len=5;
    int col_len=5;
    double ratio = 1/5.0;
    SDoublePlane row_filter(1, row_len);
    SDoublePlane col_filter(col_len, 1);
    for(int i=0; i<row_len; i++) {
        row_filter[0][i]=ratio*1;
    }
    for(int i=0; i<col_len; i++) {
        col_filter[i][0]=ratio*1;
    }
    SDoublePlane output_image = convolve::convolve_separable(input_image, row_filter, col_filter);
    SImageIO::write_png_file("c_convolve.png", output_image, output_image, output_image);
}

void testSobel(SDoublePlane& input_image) {
    SDoublePlane Sx= sobel_gradient_filter(input_image,true);
    SDoublePlane Sy= sobel_gradient_filter(input_image,false);
    SDoublePlane edgestr = edgeStrength(Sx, Sy);
    SDoublePlane tmp=binarizeImg(edgestr, SOBEL_EDGE_THRESH);
    SDoublePlane output_image=scaleBinImg(tmp);
    SImageIO::write_png_file("sobel.png", output_image, output_image, output_image);
}

vector<vector<int> > getStaves(const SDoublePlane& input_image, double* spacing) {
    SDoublePlane edgeImg = sobelEdge(input_image, SOBEL_EDGE_THRESH);
    vector<int> possLine=getPossLines(edgeImg, STAVE_LINE_RATIO);
    vector<int> squLine = getSqueezedLines(possLine, SQUEEZE_LINE_THRESH);
    *spacing = getSpacing(squLine);
    //cout<<"spacing we got is "<<*spacing<<endl;
    //------------
    // group lines
    //------------
    vector<vector<int> > gpLines=groupLines(squLine, *spacing, MAX_DIST_ALLOW);
    //-------------
    // refine lines
    //-------------
    vector<vector<int> > rfLines=refineLines(gpLines, *spacing, MIN_DIST_ALLOW);
    //for(int i=0;i<gpLines.size();i++) {
    //    cout<<"============="<<endl;
    //    printVec(rfLines[i]);
    //}
    string filename;
    filename=subDir+'/'+"staves.png";
    SImageIO::write_staff_image(filename.c_str(),rfLines, input_image);
    return rfLines;
}

void findSymbolNaive(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves) {
    vector<detectedSymbol> symbols[3];

    // binary thresh 
    int bin_thresh[3] = {BIN_THRESH_TPL1, BIN_THRESH_TPL2, BIN_THRESH_TPL3};
    string filename;

    //-------------------------------
    // do this for 3 types of symbols
    //-------------------------------
    for(int symtype = 0; symtype<3; symtype++) {
        // get the template
        SDoublePlane tmp=getTemplate(spacing,symtype+1);
        SDoublePlane tmplt=filter::flip(tmp);
        
        // template symbol height and width
        int symH=tmplt.rows();
        int symW=tmplt.cols();

        // binarize input image and template
        SDoublePlane binImg = binarizeImg(input, bin_thresh[symtype]);
        SDoublePlane binTmplt = binarizeImg(tmplt,bin_thresh[symtype]);

        // convolve binImg with binTmplt
        SDoublePlane convImg= convolve::convolve_general(binImg,binTmplt, notXor);
        // output this image for notehead
        ostringstream oss;
        oss<<"scores4_template"<<symtype+1<<".png";
        filename.clear();
        filename = subDir+'/'+oss.str();
        SImageIO::write_png_file(filename.c_str(),convImg, convImg, convImg);

        // do non-maximal suppressing for symbols
        SDoublePlane suppressImg= symNonMaxSuppress(convImg, symH, symW);

        // binarize suppressed image 
        SDoublePlane resultImg= binarizeImg(suppressImg, symH*symW*SYM_THRESH);

        // get a list of symbols
        symbols[symtype] = getSymbolList(resultImg, symH, symW,
                int(max(symH,symW)*TOO_NEAR_RATIO), symtype);
    }

    // assign pitch to symbols[0]
    assignPitch(staves, spacing, symbols[0]);

    // combines 3 symbol lists to one
    for(int i=1;i<3;i++) {
        for(int j=0;j<symbols[i].size();j++) {
            symbols[0].push_back(symbols[i][j]);
        }
    }

    filename.clear();
    filename = subDir+'/'+"detected4.png";
    SImageIO::write_detection_image(filename.c_str(), symbols[0], input);
}

void finale(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves) {
    vector<detectedSymbol> symbols[3];

    // binary thresh 
    int bin_thresh[3] = {BIN_THRESH_TPL1, BIN_THRESH_TPL2, BIN_THRESH_TPL3};
    string filename;

    //-------------------------------
    // do this for 3 types of symbols
    //-------------------------------
    for(int symtype = 0; symtype<3; symtype++) {
        // get the template
        SDoublePlane tmp=getTemplate(spacing,symtype+1);
        SDoublePlane tmplt=filter::flip(tmp);
        
        // template symbol height and width
        int symH=tmplt.rows();
        int symW=tmplt.cols();

        // binarize input image and template
        SDoublePlane binImg = binarizeImg(input, bin_thresh[symtype]);
        SDoublePlane binTmplt = binarizeImg(tmplt,bin_thresh[symtype]);

        // convolve binImg with binTmplt
        SDoublePlane convImg= convolve::convolve_general(binImg,binTmplt, notXor);

        // do non-maximal suppressing for symbols
        SDoublePlane suppressImg= symNonMaxSuppress(convImg, symH, symW);

        // binarize suppressed image 
        SDoublePlane resultImg= binarizeImg(suppressImg, symH*symW*SYM_THRESH);

        // get a list of symbols
        symbols[symtype] = getSymbolList(resultImg, symH, symW,
                int(max(symH,symW)*TOO_NEAR_RATIO), symtype);
    }

    // assign pitch to symbols[0]
    assignPitch(staves, spacing, symbols[0]);

    // combines 3 symbol lists to one
    for(int i=1;i<3;i++) {
        for(int j=0;j<symbols[i].size();j++) {
            symbols[0].push_back(symbols[i][j]);
        }
    }

    //----------------------------
    // output detected png and txt
    //----------------------------
    filename.clear();
    filename = subDir+'/'+"detected7.png";
    SImageIO::write_detection_image(filename.c_str(), symbols[0], input);
    filename.clear();
    filename = subDir+'/'+"detected7.txt";
    SImageIO::write_detection_txt(filename.c_str(),symbols[0]);
}

void findSymbolSmart(const SDoublePlane& input, double spacing, const vector<vector<int> >& staves) {
    vector<detectedSymbol> symbols[3];

    string filename;

    //-------------------------------
    // do this for 3 types of symbols
    //-------------------------------
    for(int symtype = 0; symtype<3; symtype++) {
        // get the template
        SDoublePlane tmp=getTemplate(spacing,symtype+1);
        SDoublePlane tmplt = filter::flip(tmp);
        
        // template symbol height and width
        int symH=tmplt.rows();
        int symW=tmplt.cols();

        // run edge detector on template and image
        SDoublePlane imgEdge= sobelEdge(input, SOBEL_EDGE_THRESH);
        SDoublePlane tmpltEdge = sobelEdge(tmplt, SOBEL_EDGE_THRESH);

        // compute the scoring function
        //SDoublePlane pixelImg= nearestPixel(imgEdge);
        //SDoublePlane scoreImg = convolve::convolve_general(pixelImg,tmpltEdge);
        SDoublePlane scoreImg = convolve::convolve_general(imgEdge,tmpltEdge,notXor);

        // do non-maximal suppressing for symbols
        SDoublePlane suppressImg = symNonMaxSuppress(scoreImg,symH,symW);

        // binarize suppressed image 
        SDoublePlane resultImg= binarizeImg(suppressImg, symH*symW*0.85);

        // get a list of symbols
        symbols[symtype] = getSymbolList(resultImg, symH, symW,
                int(max(symH,symW)*TOO_NEAR_RATIO), symtype);
    }

    // assign pitch to symbols[0]
    assignPitch(staves, spacing, symbols[0]);

    // combines 3 symbol lists to one
    for(int i=1;i<3;i++) {
        for(int j=0;j<symbols[i].size();j++) {
            symbols[0].push_back(symbols[i][j]);
        }
    }

    filename.clear();
    filename = subDir+'/'+"detected5.png";
    SImageIO::write_detection_image(filename.c_str(), symbols[0], input);
}
